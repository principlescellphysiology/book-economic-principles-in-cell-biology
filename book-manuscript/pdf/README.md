Book - latest compiled version
==============================

## The entire book (compiled from files in ~/book-manuscript/latex/main/)

* ```main-SCREEN.pdf``` - Standard version (only published chapters)
* ```main-PRINT.pdf``` - Standard version, typset for printing
* ```main-EBOOK.pdf``` - Standard version, layout for  e-book readers
* ```main-DRAFT.pdf``` - Draft version, all chapters inclduing drafts

## Single chapters (in subfolders)

```chapters-SCREEN``` Chapters ("book" version)

```chapters-PREPRINT``` Chapters ("preprint" version - like on zenodo)


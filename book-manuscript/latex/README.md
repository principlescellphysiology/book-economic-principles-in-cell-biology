Book - latex source files 
=========================


`main`: latex source code for compiling the entire book. To  request changes in these files, please use our [Google document](https://docs.google.com/document/d/1K9GBh0DlwDVtga_P0HgNAcdulgRHn_CgTDtZ1l64Dik/edit#heading=h.2rjfgfdcck3x)

`chapters`: latex source code for individual chapters. Each chapter is written by a team of authors. For a template files, see the subfolder `latex/TEMPLATE`. For chapter drafts, see this [Google Document](https://docs.google.com/document/d/1oOBfnEJrt-PZJAblwgY_zM5TBRiAifP9jW6w9gZap9w/edit#)

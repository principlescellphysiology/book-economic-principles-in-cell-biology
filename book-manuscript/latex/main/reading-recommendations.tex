Cellular economics - and systems biology more generally - builds on knowledge from different disciplines and on a history of ideas in biology and beyond. Below you will find a number of books, articles, and online resources that provide background information. For readings specific to individual chapters, please see the ``Recommended readings'' sections at the end of each chapter.

\subsection*{Recommended textbooks}

\comment{Idea by Pranas: we could tag the recommendations by "what you'd like to learn?"; or, alternatively, we just have a separate box "A crash course in all disciplines", where we pick one fundamental textbook per discipline and let the reader choose themselves.}

Books on \textbf{Biochemistry and cell biology}:

\textbf{Cornish-Bowden, Athel. \emph{Fundamentals of Enzyme Kinetics}}; Wiley Blackwell, 2012 (4th Edition). A detailed presentation of enzyme kinetics that also considers the broader implications of the field for areas such as systems biology and bioinformatics. The book devotes particular attention to the areas of multi-enzyme complexes and cooperativity.

\textbf{Alberts Bruce, Heald Rebecca, et al. \emph{Molecular Biology of the Cell}}; Garland Science, 2022 (6th Edition). A comprehensive textbook of cell biology which covers expression and transmission of genetic information, internal organization of cells, and behavior of cells in multicellular organisms. The book also presents experimental methods used to investigate cells and understand their behavior. Free access online (5th Edition): \url{https://archive.org/details/MolecularBiologyOfTheCell5th}.

\textbf{Swanson, Michele, S., Joyce, Elizabeth, A. and Horak, Rachel, E.A. \emph{Microbe}}; ASM Press, 2022 (3rd Edition). An introductory textbook to microbiology that incorporates both fundamental principles and accessible case studies to illustrate microbiology’s social and environmental relevance.

\textbf{Milo, Ron and Phillips, Rob et. al. \emph{Cell Biology By The Numbers}}; Taylor \& Francis, 2016. The book addresses the relative paucity of quantitative information about biological systems. The authors propose ways to quantify biological processes and objects and develop a “sense” for applicable scales and sizes. Free access online: \url{http://book.bionumbers.org/}.

\textbf{Phillips, Rob, Kondev, Jane, et al. \emph{Physical Biology of the Cell}}; Garland Science, 2012 (2nd Edition). This textbook presents molecular and cell biology through the lens of physical biology. Biological phenomena are treated as coherent systems founded on physical principles. The overarching topic is that quantitative biological intuition builds on applying few fundamental physical models, and this logic can be used to address a wide range of biological problems.
    
\textbf{Harold, Franklin M. \emph{The Way of the Cell: Molecules, Organisms, and the Order of Life}}; Oxford University Press, 2003. The text offers connections between physics, chemistry, and biology and how insights gained from integrative approach help to understand the processes and principles that make the creation of living organisms from inanimate chemicals possible.\\

Books on \textbf{Biothermodynamics and biological physics}:

\textbf{Haynie, Donald T. \emph{Biological Thermodynamics}}; Cambridge University Press, 2008 (2nd Edition). An introduction to bioenergetics focused on the physical nature of energy transformation in living cells. The book covers relevant concepts of physics, like the laws of thermodynamics, Gibbs free energy, statistical thermodynamics, binding equilibria and reaction kinetics.

\textbf{Nicholls, David, G and Fergueso, Scott J. \emph{Bioenergetics}}; Academic Press, 2013 (4th Edition). A textbook on cellular energy flows and intracellular thermodynamics. The book takes recent advances in chemiosmotic energy conversion into account; the book also covers the role that mitochondria play in the life and death of the cell.

\textbf{Nelson, Philip. \emph{Biological Physics}}; Chiliagon Science, 2020. The book covers biophysics, bioengineering and nanotechnology and highlights recent advances in molecular motors, self-assembly, and single-molecule manipulation. It also describes practical applications, and contains exercises designed to develop modeling skills of computer programming in languages such as MATLAB and Python.\\

Books on \textbf{Systems Biology}:

\textbf{Klipp, Edda, Liebermeister, Wolfram, et. al. \emph{Systems biology, A Textbook}}; Wiley-Blackwell, 2016 (2nd Edition). An overview of basic concepts in systems biology supplemented with illustrative examples and practical case studies. The book presents an integrative approach to living systems combining biology, computer science, and engineering.

\textbf{Voit, Eberhard. \emph{A First Course in Systems Biology}}; Garland Science, 2018 (2nd Edition). A moderately advanced introduction to systems biology. The book prioritizes conceptualization and development of computational models and the ways theoretical work enhances understanding of diverse biological systems.

\textbf{Sauro, Herbert. \emph{Systems Biology: Introduction to Pathway Modeling}}; Ambrosius Publishing, 2020. The book covers fundamentals of biochemical modeling, including a review on differential equations and stochastic models. The book highlights the increasingly prominent role of computer models in modern systems and synthetic biology. Book includes hands-on modeling exercises to illustrate computational models methods for designing, building, simulating models; performing stability analysis, and parameter estimation.

\textbf{Palsson, Bernhard. \emph{Systems Biology: Simulation of Dynamic Network States}}; Cambridge University Press, 2011. An introduction to the mass action stoichiometric simulation (MASS) approach to transform stoichiometric reconstructions into dynamic models making use of metabolomic and fluxomic data. The MASS approach aims at depicting integrated processes that depend on a precise accounting of small molecules and proteins.

\textbf{Alon, Uri. \emph{An Introduction to Systems Biology: Design Principles of Biological Circuits}}; Chapman \& Hall, 2006. The book presents design principles of biological systems such as stability, robustness, and optimal design, and how they can be applied to deepen the understanding of living cells. The book provides a mathematical framework that can be used to better understand and design biological circuits by highlighting the recurring circuit elements that make up biological networks.

\textbf{Szallasi, Zoltan, Periwal, Vipul, and Stelling, Joerg. \emph{System Modeling in Cellular Biology: From Concepts to Nuts and Bolts}}; MIT Press, 2010. An overview of systems modeling in cell biology. The book incorporates concepts from biology, computer science, mathematics, statistics, physics, and biochemistry and considers how they can be integrated to study biological systems. The book also covers multiple modeling paradigms and discusses their suitability for modeling different biological systems.

\textbf{Savageau, Michael, A. \emph{Biochemical Systems Analysis: A Study of Function and Design in Molecular Biology}}; CreateSpace Independent Publishing Platform, 2010. A primer to identifying and defining  biological design principles for systems like complex biochemical pathways, intricate gene regulation circuits, network interactions within the immune system, plasticity of neural networks, and the pattern formations of cellular networks. The book addresses integration of multiple viewpoints, the relation between the behavior of intact systems and their molecular components; unifying design principles that give meaning for vast diversity of alternative molecular designs; higher-level theory and quantitative prediction.\\

Books on \textbf{Metabolic control}:

\textbf{Heinrich, Reinhart and Schuster, Stefan. \emph{The Regulation of Cellular Systems}}; Springer, 1996. The book covers the mathematical analysis of enzymatic systems such as stoichiometric analysis, enzyme kinetics, dynamical simulation, metabolic control analysis, and evolutionary optimization.

\textbf{Fell, David. \emph{Understanding The Control of Metabolism}}; Frontiers in Metabolism Ser. No. 2; Ashgate Publishing Company, 1997. The book offers a comprehensive theory of metabolic regulation, metabolic control analysis, that encompasses the philosophical underpinnings of regulation and control concepts. It also reviews and summarizes experimental methods that add a new dimension to the study of metabolism and its regulation in complete multicellular organisms, isolated tissues, organs and individual cells.

\textbf{Sauro, Herbert. \emph{Systems Biology: An Introduction to Metabolic Control Analysis}}; Ambrosius Publishing, 2018. This book provides an introduction to the field of Systems Biology and demonstrates how  computational models can be used to understand the dynamics of  complex biological systems and how the individual components that comprise those  systems interact to produce complex behaviors.  The book details the fundamentals of modeling biological pathways, such as metabolic, signaling, and gene regulatory networks and instruction in the application of tools that can create those models.\\

Books on \textbf{Mathematics}:

\textbf{Slivanus, P. Thompson and Gardner, Martin. \emph{Calculus Made Easy}}; St. Martin’s Press, 1998 (revised/expanded edition). An introduction to Calculus for beginners and those with no previous background. Essentials of differentiation and integration are covered: the power rule, product rule, and chain rule for differentiation, as well as basic techniques of integration. The book takes a different approach to calculus by emphasizing an intuitive understanding of calculus rather than insisting on a rigorous formalism. The applications-oriented approach highlights the applicability of those concepts to real-world cases. Free pdf at \url{https://calculusmadeeasy.org/}.

\textbf{Johnston, Nathaniel. \emph{Introduction to Linear and Matrix Algebra}}; Springer, 2021. The book introduces key concepts of linear algebra, such as vectors, matrices, systems of linear equations. The book primarily covers vector spaces and their subspaces, and linear transformations between vector spaces. The book is full of examples in which linear algebra is used in fields such as computer science, physics, and engineering, e.g., least-squares solutions, Markov chains, and cryptography.

\textbf{Strang, Gilbert. \emph{Introduction to Linear Algebra}}; Wellesley-Cambridge Press, 2023 (6th Edition). The book covers theory and applications of vector algebra and geometry, systems of linear equations, vector spaces and subspaces, orthogonality, determinants, and eigenvalues and eigenvectors, and linear statistical analysis. Later chapters concern contemporary applications and computational issues in linear algebra.

\textbf{Strang, Gilbert. \emph{Calculus}}; Wellesley-Cambridge Press, 1991 (2nd Edition). This book is a thorough review of both single-variable and multivariable calculus. The book makes use of visual methodologies (diagrams) and illustrative examples to aid understanding and enhance the learning experience. Examples highlight the application of calculus to a variety of domains such as physics and engineering and economics.

\textbf{Lay, David, Lay, Steven, and McDonald, Judi. \emph{Linear Algebra and Its Applications}}; Pearson, 2014 (5th Edition). The book provides a basic introduction to linear algebra, illustrated by a broad selection of case studies. The emphasis is on developing a conceptual view of the subject and an understanding of why, and not just how, certain methods are used. Problems and cases presented demonstrate how linear algebra can be applied to various fields.

\textbf{Strogatz, Steven. \emph{Nonlinear dynamics and chaos: With applications to physics, biology, chemistry, and engineering}}; CRC Press, 2000. An exploration of concepts related to dynamical systems and nonlinear behaviors that exhibit unpredictable or chaotic behavior and cannot be easily predicted by simple equations. The book covers the basics of dynamical systems, characterizing the properties of non-linear systems and the points of equilibrium. The book also describes temporal dynamics of systems. Finally, it considers fractals and unique properties of chaotic systems with practical examples.\\

Books on \textbf{Information theory}:

\textbf{Cover, Thomas A. and Thomas, Joy A. \emph{Elements of Information Theory}}; Wiley Series in Telecommunications and Signal Processing; Wiley-Interscience, 2006 (2nd Edition). The book considers the theoretical underpinnings and practical application of information theory. It also provides a thorough review of information theory’s constituent elements: entropy, data compression, channel capacity, rate distortion, network information theory, and hypothesis testing.

\textbf{Bialek, William. \emph{Searching for Principles}}; Princeton University Press, 2012. The book explores the intersection of theoretical physics and biology and argues that the fundamental principles of physics can be applied to living systems and used to better understand their behavior. The book advocates for an interdisciplinary approach that integrates insights from physics, biology, and mathematics to develop a deeper understanding of how living organisms behave and function.\\

Book on \textbf{Economics}:

\textbf{Core ECON}. \emph{Economics for a Changing World}. A free textbook offering a new approach to teaching and learning economics, supported by an extensive collection of online resources. Written by a global collective of economists. All content is freely available at \url{www.core-econ.org}.

\subsection*{Classical books, relevant to cell biology and the “economy of the cell”}

\textbf{Brasier, Martin. \emph{Secret Chambers: The Inside Story of Cells and Complex Life}}; Oxford University Press, 2012. This book draws on insights from paleontology, biology, and history of science in its exploration of the evolution of complex life on Earth.  It focuses specifically on how cells developed the capacity for organization and complexity using an investigation into the origins of eukaryotic cells,  the fundamental elements of all complex life forms, as illustration.

\textbf{Zimmer, Carl. \emph{Microcosm: \textit{E. coli} and the New Science of Life}}; Knopf Doubleday, 2009. This book describes the role \textit{E. coli} has played in defining the history of biology - from the discovery of DNA to more recent advances in biotechnology. The book  highlights the bacterium’s malleability, mutability, and survivability. Methods which exploited \textit{E. coli}’s inherent malleability have modified and repurposed the bacterium to produce a range of beneficial products from drugs to fuels. Because it mutates in near real-time, \textit{E. coli} has also been a source of insight into the process of evolution. Detailed examination of its genome has revealed a record of billions of years of evolutionary history. Finally, the book highlights the numerous strategies \textit{E. coli} uses to survive, from practicing chemical warfare to building microbial cities.

\textbf{Monod, Jacques. \emph{Chance and Necessity: An Essay on the Natural Philosophy of Modern Biology}}; Vintage Books, 1972. Monod argues that life should be understood as a result of natural processes that do not adhere to a predetermined purpose or follow a predefined design: it arose by chance and that was then conditioned by natural selection. Hence the book’s central thesis is that the observed evolution of life and its diversity is the product of “Chance" - random mutations in genetic material and "Necessity" the deterministic laws of physics and chemistry that govern these mutations. Together, “Chance” and “Necessity” explain the evolution and diversity of life.

\textbf{Prigogine, Ilya and Stengers, Isabelle. \emph{Order Out Of Chaos}}; Bantam, 1984. The text reconciles order with chaos and synthesizes concepts of time and chance to form a lens through which thermodynamics can be appreciated and its attendant laws can be better understood. In doing so, it offers a provocative view of the universe and novel insights into humankind’s position within it.

\textbf{Harold, Franklin M. \emph{In Search of Cell History: The Evolution of Life’s Building Blocks}}; University of Chicago Press, 2014. The book presents research into the history of the cell and the debate conclusions from that research have generated while describing the evolution of cellular organization, the origin of complex cells, and the incorporation of symbiotic organelles.

\textbf{Lyons, Sherrie L. \emph{From Cells to organisms: re-envisioning cell theory}}; University of Toronto Press, 2020. The book integrates the history of science with fundamental biological concepts and provides context that draws on the nature of scientific practice and rise of ideas. The book shows how discoveries, debates, and scientific processes regarding understanding of cells and organisms shaped modern biology. The focus on controversy highlights the iterative nature of science, showing how scientific knowledge evolves through argument, testing, and reinterpretation of evidence. The book connects past and present by describing how cell theory laid the foundation for the development of fields such as molecular biology, biochemistry, and genetics.

\textbf{Niklas, Karl J. \emph{Plant Allometry: The Scaling of Form and Process}}; University of Chicago Press, 1994. The author seeks to apply allometry to studies of plant evolution, morphology, physiology, and reproduction and shed light on the relationship between organ size and plant form and physiology.

\textbf{Rosen, Robert. \emph{Optimality Principles in Biology}}; Springer, 1967. The text concentrates on conceptual and theoretical aspects of cost functions and the decisive role they play in the achievement of optimality in biological systems. It focuses in particular on the relationship between extremization and stationarity, and the significance of the necessary conditions for stationarity.

\textbf{Rosen, Robert. \emph{Life Itself: A Comprehensive Inquiry Into the Nature, Origin, and Fabrication of Life}}; Columbia University Press, New York, 1991. A treatise on complex biological systems that calls into question “reductionism”, the belief that all complex systems can be broken down (reduced) to their constituent parts. The goal of this approach is to effect more rigorous analysis.

\textbf{Schr\"{o}dinger, Erwin. \emph{What is life? With Mind and Matter and Autobiographical Sketches}}; Canto Classics (Cambridge University Press reprint), 2012. The book investigates the nature of life from the physics perspective and speculates on the age-old question of the relationship between mind and matter. Throughout the book, the author attempts to reconcile biology and quantum physics, in order to demonstrate how seemingly chaotic processes of living organisms in fact obey the laws of physics while maintaining order and stability. Some of the hypotheses articulated in the book laid the conceptual foundation for molecular biology, e.g. the supposition that the key to life lies in molecular structures that store and transmit information.

\textbf{Cornish-Bowden, Athel. \emph{The Pursuit of Perfection: Aspects of Biochemical Evolution}}; Oxford University Press, 2004. The book explores the interplay of biochemistry and evolutionary biology, the two being implicitly connected and that each can only be understood fully in the context of the other. The book also argues for a more balanced approach to science: in light of Nature’s complexity and unpredictability, perfect models are unattainable; we should strike a balance between accuracy and practical application. Diversity of biological systems is a prime example of this. The book examines how natural selection optimizes biochemical processes and enzymes but stops short of achieving absolute perfection due to constraints like energy efficiency, environmental fluctuations, and genetic variation.

\textbf{West, Geoffrey. \emph{Scale: The Universal Laws of Growth, Innovation, Sustainability, and the Pace of Life in Organisms, Cities, Economies, and Companies}}; Weidenfeld \& Nicolson, 2017. This book presents the theory of scale that is grounded in the realization that universal mathematical laws govern complex systems in nature, society, and economics. These so-called scaling laws reflect underlying efficiencies and constraints based on how energy, resources, or information are distributed across networks. The book draws from biology, physics, and complexity theory to explain the mathematical principles that govern the scaling of both living systems and social organizations: cities, companies, and organisms.

\textbf{Dawkins, Richard. \emph{The Blind Watchmaker: Why the Evidence of Evolution Reveals a Universe Without Design}}; Norton \& Company, 1986. Dawkins makes the case for the theory of evolution through natural selection and in doing so refutes the intervention of the divine (metaphorically referred to as the “Watchmaker”). The book highlights the differences between human design and planning and the way natural selection works. Drawing a distinction with the original metaphor for the presence of the divine, the book concludes that the evolutionary process, on further investigation, reveals itself to be more similar to that of a blind watchmaker.

\subsection*{Classical articles that shaped the thinking in our field}

\textbf{Jacob, François. \emph{“Evolution and Tinkering”}}; Science, volume 196, issue 4295, 1977. The article elucidates the concept of evolution by viewing it through the lens of “tinkering”. It proposes that consideration of the cumulative effects of history on the evolution of life leads to an alternative account of the patterns that characterize the history of life on earth.

\textbf{Gould, Stephen Jay, Lewontin, Richard. \emph{The spandrels of San Marco and the panglossian paradigm: a critique of the adaptationist programme}}; Proceedings of the Royal Society B: Biological Sciences, 205(1161), 581–598, 1979. This paper is a critique of the adaptationist view of evolution that the authors illustrate by drawing an zanal;ogy  to an architectural feature called the spandrel. The adaptationist school holds that most biological features adaptations and that these adaptations are the result of evolutionary pressures that have favored their development because they offer some advantage to the organism in its environment. Countering the adaptationist view, the authors of the critique posit that just as the spandrel, a triangle-shaped space between arches in structures like the Basilica of San Marco, is a byproduct of the arches and domes that are the Basilica’s constituent elements  but can be artistically decorated, making them look intentional many biological traits may be byproducts of other structural necessities, not direct adaptations.   In fact, these traits may not serve any specific adaptive purpose but have come into being as a result of constraints, like the need to maintain structural integrity,  or as side effects of other adaptations.

\textbf{Lazebnik, Yuri. \emph{Can a biologist fix a radio?— Or, what I learned while studying apoptosis}}; Cancer Cell, 3, 179-182, 2002. The paper offers a critique of the way systems biology, in particular, approaches the study of living organisms through the analogy of the way an engineer approaches the diagnosis and repair of a broken radio. Engineers typically understand systems holistically while the approach of a biologist is characteristically reductionist. The reductionist approach to a malfunctioning radio is to isolate and count radio’s components without fully understanding how they interact with one another as parts of a larger, integrated system. The paper argues that biologists should follow the example of engineers in taking a more integrative and systems-based approach: an understanding of how an entire system functions is the key to identifying and diagnosing problems. The approach the paper advocates takes the form of improved models and frameworks that enable a better understanding of the complex interactions that characterize biological systems.

\textbf{Newsholme, Philip. \emph{Mapping life’s reactions: a brief history of metabolic pathways and their regulation}}; The Biochemist Volume 31, Issue 3, 2009. The article provides a historical review of discovery of metabolic functions and assembling them into metabolic pathways. This period begins in the early 20th century with a highlight of contributions made by German chemists such as Meyerhof and Krebs. The paper charts advances in the understanding of metabolic regulation and the refinement of mathematically-grounded methods for accurately and adequately modeling metabolic pathways from the post-war era. Finally, it extends the account into the current time with a description of the advent of metabolomics and the evolution of systems biology.

\subsection*{Recommended online videos and podcasts}

\textbf{Milo, Ron. \emph{Lectures on “Cell biology by the numbers”}} The lectures, accompanying the book “Cell biology by the numbers”, invite viewers to think about cells quantitatively. Ron Milo shows that knowing a few important numbers (or looking them up in the BioNumbers database) and using them for rough estimates gives an intuitive sense for how cells function!

TEDx talk “A sixth sense for understanding our cells” \url{https://www.youtube.com/watch?v=JC7WnzM2Lsc} \\
Full BioNumbers lectures on \url{https://www.youtube.com/channel/UChCzuzoZp5NheAPWH5YoGWw}

\textbf{Lercher, Martin J., and Yanai, Itai. \emph{Night Science.} Series of editorials in Genome Biology (Springer Nature) 2019-2022, and podcast 2021-ongoing.} The title, "Night Science", draws on a term coined by the biologist Francois Jacob to characterize the significant parts of scientific research that occur “behind the scenes”. This iteration on the original concept charts the decline of innovation and creative thinking in Science in favor of a focus on near-term results and financial gain. It also offers prescriptions for restoring the creative spirit to science and revitalizing innovation.

Website: \url{https://night-science.org/} \\
Materials: \url{https://www.biomedcentral.com/collections/night-science} \\
Podcast on Spotify: \url{https://open.spotify.com/show/6berzd2rX6rpJQ6CPnbOtI?si=2176cbb795f24c6a}

\textbf{Khan Academy. \emph{Free Math Courses.}} Khan Academy offers free math courses, including advanced topics like differential equations, linear algebra, and multivariable calculus. These courses feature video tutorials, practice problems, and step-by-step solutions, making complex topics like systems of equations, vector spaces, and partial derivatives easier to grasp. The lessons are self-paced and adaptable.

Website: \url{https://www.khanacademy.org/}

\comment{More elements (to be discussed or added later)
 
o Population genetics and evolution: Principles of population genetics, Daniel Hartl + Andrew Clark\\
o Predecessor book of “Microbe”: (according to frank, much better than “Microbe”,): “Physiology of the bacterial cell”\\
o Book: Huxley, J.S., “Constant differential growth ratios and their significance, Nature (London), 114:895-96\\
o Also Proposed by Frank: WIlliam Bialek: good videos on biophysics and modeling (tip by frank)\\
o Monod’s Nobel prize lecture, Enzyme biochemistry, allosteric regulation, the concept of the operon, diauxic growth
}

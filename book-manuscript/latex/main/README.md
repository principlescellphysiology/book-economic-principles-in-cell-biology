Book - central files 
====================

This folder contains the up-to-date version of files shared between all the chapters

* main.tex (main latex file for compiling the book)
* style.tex
* macros.tex 
* bibliography.bib

The files will be updated according to changes requested in our [Google document](https://docs.google.com/document/d/1K9GBh0DlwDVtga_P0HgNAcdulgRHn_CgTDtZ1l64Dik/edit#).

\section{Equations for birth size}
\label{MET:Sec:EquationsForBirthSize}

Here we derive the dynamic equations of the
birth size $q_0^i$ across generations (indexed by $i$) in the
discrete-time formalism.  We define $\langle q_0\rangle_\alpha$ as the
average value of $q_0$, and the log size deviation
$\delta q^i_0 := q_0^i - \langle q_0\rangle_\alpha$. The dynamics for
the log-size deviation takes the form
\begin{equation}
\delta q_0^{i+1} = g( \delta q_0^i, \alpha ) + \zeta^i( \delta q_0^i, \alpha )    \ ,
\end{equation}
where $\zeta^i( \delta q_0^i, \alpha ) $ is a random variable with
zero mean. This equation has the same degree of generality of
Eq.~(\ref{eq:discgen}) and can express any arbitrary division control
model (or equivalently any shapes of the hazard rate function). In
order to make further mathematical (and biological) progress, we need
to simplify the equation and make the comparison with data
possible. There are several possible choices. In the following, for
simplicity, we first neglect the fluctuation of the growth rate
$\alpha$. Assume that the size at birth is the only variable
influencing cell division ($g(\cdot)$ is a function of $\delta q_0^i$
only) will allow us to introduce a linear-response framework. We will
then describe how to consider the heterogeneity of multiple growth
parameters.

The main empirical observation that comes to our help is the fact that
the coefficient of variation of $q_0^i$ is small (typically around
0.15)~\cite{Jun2018,Taheri-Araghi2015,Amir2014,Kennard2016,Grilli2017}. The
small value of the coefficient of variation strongly suggests the
possibility of Taylor-expanding the function $g( \delta q_0^i)$ around
$\delta q_0^i = 0$~\cite{Amir2014}. In this limit, the function
$g( \delta q_0^i ) $ is approximately linear and the random variable
$\zeta^i( \delta q_0^i, \alpha )$ can be well approximated by a
Gaussian random variable with zero mean and constant
variance~\cite{Grilli2017}. The resulting equation reads
\begin{equation}
\delta q_0^{i+1} = (1- \lambda) \delta q_0^i + \sigma \xi^i    \ ,
\label{eq:linfin}
\end{equation}
where $\xi^i$ is a Gaussian random variable with zero mean and unit
variance. The two parameters $\lambda$ and $\sigma$ encode,
respectively, the relevant information about the mechanism of size
control and the level of stochasticity. The parameter $\sigma$ simply
corresponds to $\zeta^i( 0, \alpha )$.  The parameter $\lambda$, which
quantifies the strength of size control, has a direct relationship
with the mechanism at its origin. It is defined as
$\lambda = 1 - g'(0,\alpha)$. For instance, the sizer corresponds to
$\lambda = 1$ and an adder to $\lambda = 1/2$. The case $\lambda =0$
does not lead to a stationary process and corresponds to a
timer. Consequently, this parameter can easily be inferred from the
plots in Figure~\ref{fig:introfig}.

Eq.~(\ref{eq:linfin}) can be solved analytically~\cite{Grilli2017}. In
particular one can show that the conditional probability of observing
a log-size deviation $\delta q_0^i$ from the average at generation $i$
given a deviation at generation $0$, is a Gaussian with mean
\begin{equation}
\langle \delta q_0^{i} \rangle_{\delta q_0^0}  = (1- \lambda)^i \delta q_0^0   \ .
\end{equation}
This result clearly shows how different mechanisms correspond to
different strengths of cell-size homeostasis, leading to fluctuations
persisting across a different number of generations. For a sizer,
$\lambda = 1$, the expected deviation of the daughter cell is
independent of the mother cell fluctuations. A timer, with
$\lambda = 0$, does not lead to homeostasis, as the expected deviation
of size at birth of a daughter cell is the same as the deviation of
the mother. The adder, $\lambda = 1/2$, leads on average to a halving
of the size at birth deviation at each generation, as approximately
observed in experiments~\cite{Taheri-Araghi2015}.


One can generalize the linear-response framework to consider
fluctuations of different growth parameters~\cite{Grilli2018}. In
general, one can assume that the size at birth of the daughter cell
depends on both size at birth of the mother and her individual growth
rate fluctuations.
\begin{equation}
\delta q_0^{i+1} = (1-\lambda_{qq} ) \delta q_0^{i} - \lambda_{q \alpha} \delta \alpha^i + \xi^i_q \ .
\label{eq:qlinear}
\end{equation}
Along the same lines, one can assume that the growth rate
fluctuations obey a similar equation
\begin{equation}
\delta \alpha^{i+1} = -\lambda_{\alpha q}  \delta q_0^{i} - \lambda_{\alpha \alpha} \delta \alpha^i + \xi^i_\alpha \ .
\label{eq:alphalinear}
\end{equation}
This kind of equation can be written in multiple forms, \emph{i.e.}
including multiple variables. For example, one can write an equation
explicitly for the elongation rate between divisions
$\delta G:= \delta q_0^{i+1} - \delta q_0^{i}$ or for the division
time. Since the linear-response equations assume that the fluctuations
around the means of these variables are small, all these choices turn
out to be mathematically equivalent.
%
This is also the reason why the different plots in
Figure~\ref{fig:introfig} are equivalent.
%
While a linear dependency of growth rate $\alpha$ and division time
$\tau_d$ on (log-)size at birth $q_0$ would induce a non linear
dependency of the elongation $G = \alpha \tau_d$ on the initial size,
such non-linearities can be neglected in the limit of small
fluctuations, leading always to linear
dependencies~\cite{Amir2014,Grilli2018}.

The values of the parameters $\lambda_{ab}$ can be easily inferred
using the standard tools of linear regression. Notably, the best
(maximum likelihood) estimates of these parameters can be directly
obtained from the variable covariances~\cite{Grilli2017,Grilli2018}. For instance,
$\langle \delta q^{i+1} \delta q^{i} \rangle = \lambda_{qq} \sigma_q^2
+ \lambda_{q\alpha} \langle \delta \alpha^{i} \delta q^{i} \rangle
$. By writing the expressions for other correlations (e.g.,
$\langle \delta q^{i+1} \delta \alpha^{i} \rangle$ or
$\langle \delta \alpha^{i+1} \delta q^{i+1} \rangle$ ) one can map the
coefficient $\lambda_{ab}$ with the measured covariances.

\section{Growth laws}
\label{MET:Sec:GrowthLaws}

\textbf{Growth laws and trade-offs between protein sectors.}  Prototypical predictions are the so-called "growth laws", general quantitative relationships linking proteome composition and rates of cellular processes. The reason why relationships of the kind $\lambda = \lambda(\phi_R, \phi_X, \dots)$ and $k_X(\phi_R, \phi_X,\dots)$ naturally emerge in the framework is due to cell growth and division rates being coupled to proteome allocation dynamics.

\textit{Growth law for the ribosome sector.} For example, the first growth law, stating that the ribosome mass fraction increases linearly with the nutrient-imposed growth rate, that is $ \lambda = \lambda (\phi_R) = \mathcal{K} (\phi_R - \phi_R^{min}) $, is obtained straightforwardly by noting that upon differentiation of Eq.~(\ref{eq:micromacro}) with respect to time and substitution of Eq.~(\ref{eq:macro}) and Eq.~(\ref{eq:micro}) one finds the dynamical relation $\lambda (t) =  \frac{k_n P(t)}{M}$, which at equilibrium reads (neglecting degradation) \begin{equation}
  \lambda^* =
  \frac{k_n P^*}{M} = \frac{a k_t R^* f_a}{M} = \frac{a k_t}{m_R} \frac{M_{prot}}{M} \left(\phi_R - \phi_R^{min}\right), \label{eq:k_N}
\end{equation} 
since at equilibrium the amino-acid import flux $k_n P^*$ matches the biosynthesis flux $ a k_t R^* f_a $ ($dA/dt = 0$ in absence of degradation). Note that we have used the definitions $\phi_i \equiv (m_i P_i)/M_{prot} = (m_i P_i)/(M-M_a)$ and $Rf_a = R^{active} = R - R^{inactive}$ and we have identified $\phi_R^{inactive} = \phi_R^{min}$.

\textit{Trade-Offs between Ribosomes
and Division Protein Synthesis.} Following Refs.~\cite{Serbanescu2020, Serbanescu2021}, we re-write Eq.~(\ref{eq:k_N}) as $ k_n = \frac{ak_t}{m_R} m_P \frac{\phi_R-\phi_R^{min}}{\phi_P}$ and use the constraint  $\phi_R^{max} = 1-\phi_Q = \phi_R + \phi_P + \phi_X$ to obtain \begin{equation} \phi_X = - \frac{K_n+K_t}{K_n} \phi_R + \frac{K_t \phi_R^{min} + K_n \phi_R^{max}}{K_n},
  \label{eq:serbanescu7}
\end{equation}where $K_n \equiv k_n / m_P \ ([K_n] = [T]^{-1})$
and $K_t \equiv a k_t /m_R \ ([K_t]=[T]^{-1})$. Eq.~(\ref{eq:serbanescu7}) shows a negative correlation between the ribosome and division sectors under nutrient or translational perturbations, in agreement with recent published data~\cite{Mori2021}. Also, since the rates of growth and division protein synthesis are respectively proportional to the ribosome and the division sector, this negative correlation reflects a trade-offs between allocating ribosomal resources towards growth or division (see Fig.1F in Ref.~\cite{Serbanescu2020}). 

\textit{Growth law for the division sector.} So, the larger the fraction of ribosomes making division proteins the smaller the fraction of ribosomes making ribosomes. In other words, there is a negative correlation between the growth rate and the division protein sector. Indeed, the ribosome sector is related to the growth rate via the first growth law $\phi_R = \frac{M \lambda}{M_{prot} K_t} + \phi_R^{min}$, but it is also related to $\phi_X$ via Eq.~(\ref{eq:serbanescu7}) $\phi_R = \frac{K_t \phi_R^{min}+K_n \phi_R^{max}}{K_n+K_t} - \frac{K_n}{K_n+K_t} \phi_X$. Equating the two terms yields \begin{equation}
  \lambda = \frac{K_n K_t}{K_n+K_t} \frac{M_{prot}}{M} \left( \phi_R^{max} - \phi_R^{min} - \phi_X \right), \label{DIVGL}
\end{equation}which is Eq.~(9) in Ref.~\cite{Serbanescu2020}.

We now discuss how two known steady-growth size-related behaviors emerge in the unified framework from the interplay between cell growth and cell division. 

\noindent \textbf{Adder mechanism}. As we discussed, \emph{E.~coli} cells regulate their size by adding a
constant volume between consecutive cell divisions (adder
mechanism). In a previous problem, we investigated with numerical simulations the range of validity of
this property. In the following one, we instead show analytically that the adder property is naturally embedded in the unified framework.

It can be seen then that whenever $\lambda \gg d_X/m_X$ (e.g. fast growth conditions), $\Delta s_{\mathrm{1 cycle}} \approx \frac{\lambda}{k_X} X_{th}
  = \mathrm{const}$ which is the adder property. Notably, in increasingly slower growth conditions, where degradation becomes with
the growth rate, deviations from the adder are predicted, up to the point $\lambda \ll d_X/m_X$ where $s_d \approx X_{th}d_X/(k_X m_X) = const$.

\noindent \textbf{``Schaechter–Maaloe–Kjeldgaard'' (SMK) growth law}. According to this law, the population-averaged cellular size scales with growth rate in an approximately exponential fashion~\cite{Schaechter1962}. Interestingly, deviations from the exponential trend have recently been reported, particularly at slow growth, leading to a different proposition~\cite{Zheng2020}. Notably, deviations from this law are accounted in our framework. Indeed, in an exponentially expanding population the average cell size can be expressed as $\langle s \rangle = 2 \log 2 \langle s_0 \rangle$~\cite{Jun2018}, which, combined with Eq.~(\ref{eq:threshold}) and
$\langle s_d \rangle = 2 \langle s_0 \rangle$ leads to
\begin{equation} \langle s \rangle = \frac{\lambda + \frac{d_X}{m_X}}{\tilde{k}_X
    \left(2-2^{-\frac{d_X}{m_X \lambda}}\right)} \label{SKM}
\end{equation}where, following Ref.~\cite{Serbanescu2020}, we have defined $\tilde{k}_X \equiv k_X/(2\log 2 X_{th})$. Note that since $\lambda \propto \phi_R$ and $k_X \propto - \phi_R$ the average cell size increases with ribosome abundance, a trend observed in experiments. Notably, upon determining the model parameters and making full explicit the growth rate dependence, the authors in Ref.~\cite{Serbanescu2020} with no further fitting showed that Eq.~(\ref{SKM}) recapitulates the experimental data~\cite{Zheng2020,Serbanescu2020}, a remarkable achievement of the unified framework.

\noindent \textbf{Non-steady relationships.} Finally, we contextualize within the unified framework some predictions of a
model recently proposed to unify cell division and growth in
\emph{non-steady} growth conditions~\cite{Panlilio2021}.  As we saw,
although there is consensus on an inter-division adder at the
phenomenological level, the mechanisms regulating cell division
dynamics in the bacterium \emph{E.~coli} are still widely debated. In
particular several mechanistic models based on different mechanisms
for division control were proposed for the
adder~\cite{Harris2016,Basan2015,Si2019,Wallden2016}. In order to help
selecting different scenarios, experiments beyond steady-state growth
help comparing the specific causal relationships underlying different
models with data.
%   
Following this philosophy, and aiming to shed more light on cell
division dynamics, \citet{Panlilio2021} ran multiple long-term
\textit{E.~coli} microfluidics experiments jointy monitoring
size-division dynamics and reporters of ribosomal and constitutive
genes through nutritional up-shifts. The fluorescent reporters can be
seen as proxies for the dynamics of the $R$ and $P$ sectors during the
shift. Remarkably, in their experiments they observed highly-complex
multiple-timescale dynamics in different cell-division variables
(particularly inter-division time, division rate, added volume and
added-to-initial volume ratio) during the nutritional up-shift.
Notably, in spite of this complex dynamics, they found the division
control strategy to be unaffected by the shift. The transient observed
division dynamics in their shift data falsifies several scenarios,
such as the Harris-Theoriot septum-limited division and the classic
scenario of replication-limited division. Instead, the authors found
that a threshold accumulation model such as the one described by
Eq.~(\ref{eq:macro}) could not be falsified,
\begin{equation}
  \frac{ds(t)}{dt} = \alpha(t) s(t)
  \qquad \qquad
  \frac{dN(t)}{dt} = r_X(t) s(t) \ .
  \label{eq:miamodel}
\end{equation}
This the usual scenario where a \emph{constitutive} $X$-sector protein
accumulates to a threshold value $N^*$ and at that point triggers cell
division. The regulation of cell division from a constitutive sector
is coherent with the observation that ppGpp is a cell size and cell
division regulator~\cite{Bueke2022}.
%
% Using the data for the $P$-sector and $R$-sector proxies, they could
% test whether the putative divisor protein could follow one of these
% two trends.  Assuming the size-specific production rate $r_X(t)$ of
% the divisor protein being proportional to the rate of expression of
% the constitutive or ribosomal promoter, they could run simulations of the
% model and compare them with experimental data. They found that while
% robustness of near-adder behavior across the nutrient shift was
% captured in several alternative models, the only model that reproduces
% all the observed dynamics through the nutrient shift was the one with
% a constitutive ($P$-sector) divisor protein. Hence, current data lead
% to conclude that the division process must be under the control of a
% protein in the $P$ sector (or in other words the $X$ sector behaves as
% the $P$ sector over nutrient shifts).
%
These results are also in line with independent conclusions based on
steady-state data~\cite{Serbanescu2021,Si2019,Ojkic2019} and isolate
FtsZ as a likely candidate cell-division trigger, although the
previous section has clarified how the complexity of the decision to
divide is likely higher than described by the chromosome-agnostic
cell-division models that are used in integrated frameworks. Future
efforts will have to integrate this complexity in a description that
also accounts for the interplay of different processes relevant for
cell cycle progression with cell growth.

% MCL forse non e' necessario descrivere l'ulteriore test.
%
%
% To further test and validate these findings, the authors followed a
% reversed-argument based on the same model. The argument assumes that
% $N^*$ is constant in the two stationary conditions (as observed
% throughout their experiments) and infers $r(t)$ using data on growth
% rate $\alpha(t)$ and added volume by means of the following
% equation \begin{equation} r(t) = \frac{\alpha(t) N^* }{\Delta (t)}
% \end{equation}which we already encountered in this section
% (eq.~\ref{eq:threshold}). They found that the inferred behavior of
% $r(t)$ closely resembles that of the constitutive promotoer in their
% data. 
   
% \textcolor{red}{Add comment on the fact that also in theSerbanescu
%   Work, the divisor proteins seems to be in the P-sector.}
% \end{itemize}
%% \comment{MC: I have to make sure notations are consistent in this box.}
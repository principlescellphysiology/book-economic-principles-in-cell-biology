Book chapters - latex source files
==================================

Each subfolder contains the latex source files for one of the chapters.

The folder `TEMPLATE` contains template files, which you can use to start writing your own chapter. Once you have a (preliminary) version of your chapter, please push it to the corresponding folder or send a zip file to Markus Köbis.

The folder `LATEX_INSTRUCTIONS` contains a latex document that describes and exemplifies our specific ways of using latex commands in the book project. 

## Chapter titles and shortcuts

* Preface (`PRE`)

Part 1: Cells and metabolism
* 1. The cell as a factory (`FAC`)
* 2. Cell models and optimality (`CMO`) 
* 3. A census of cell components (`CEN`)
* 4. A dynamic picture of metabolism (`MET`) 
* 5. Balanced growth (`BAL`)

Part 2: Metabolic models
* 6. Constraint-based / stoichiometric modelling (`CBM`)
* 7. Optimisation in flux space / Flux-balance analysis models (`FLX`)
* 8.  Enzymatic efficiency and the choice between metabolic pathways (`PAT`)
* 9. Cost-optimal metabolic states (`OME`)

Part 3: Cell models
* 10. Coarse grained models of cellular growth (`SMA`)
* 11. Large cell models (`LAR`)
* 12. Fitness costs and benefits of protein expression, single-protein fitness landscapes (`PRF`)

Part 4: Dynamics, anticipation, cell communities, and evolution
* 13. Optimal behaviour in time (`TIM`)
* 14. Can cells reach optimal states by self-optimising protein expression? (`REG`)
* 15. Variability and uncertainty (`VAR`)
* 16. Population and community models (`POP`)
* 17. Evolution, fitness, and optimality (`FIT`)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    please do not edit this block   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifx\weAreInMain\undefined           %
\documentclass[a4paper]{book}        %
\def\mainfolder{./main}              %
\IfFileExists{main/macros.tex}       %
  {\input{main/macros.tex}}          %
  {\input{../../main/macros.tex}}    %
\IfFileExists{main/style.tex}        %
  {\input{main/style.tex}}           %
  {\input{../../main/style.tex}}     %
\input{./macros-chapter.tex}         %    
                                     %
\begin{document}                     %
  \renewcommand{\chapterroot}{./}    %
\fi                                  %
%                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter*{Overview of the book}
\addcontentsline{toc}{chapter}{Overview of the book}  
\label{OVE}

%\comment{\begin{center}
%  {\large\textbf{Version: \today}}%
%\end{center}}

%\includechapterauthors{
%  FirstName1 LastName1\affiliationnum{1}\orcid{XXXX-XXXX-XXXX-XXXX},
%  FirstName2 LastName2\affiliationnum{2}\orcid{YYYY-YYYY-YYYY-YYYY}
%}
%\affiliations{
%  $^1$ Affiliation 1, City 1, Country 1\\ $^2$ Affiliation 2, City 2, Country 2
%}

%\chapterkeywords{}

\chapteracknowledgments{The overview chapter has been planned and outlined by W.~Liebermeister, edited by Paul Ross and Michela Pauletti, and reviewed by Diana Sz\'eliov\'a. The graphics were designed and made by Michela Pauletti.}

%\chaptercitation{X.~Author1 and Y.~Author2}{Chapter title}{PUT-THE-DOI-HERE}
%

\section*{Getting started} 
\addcontentsline{toc}{section}{Getting started}  

\subsection*{What is this book about?}

In biology, the "economy of the cell" has become increasingly central as a way of understanding cells. In particular, it has been used as a perspective on metabolic states, the allocation of protein resources in cells, and the interplay between production processes and cell growth. In this book, we focus on diverse biological topics of interest and, where possible, use economic analogies to show that, much like in human economics, balance and resource management are crucial for cells. The “economy of the cell” is based on, and is a part of, systems biology, a branch of biology that is typically concerned with networks, large cell biological data sets, and dynamic models. 

\comment{clarify the distinction between the “mechanistic view” that explains cell behavior by regulation and is predominant in systems biology, and the “economic view” that asks what behavior is useful or “profitable” for a cell, and that is sometimes used in systems biology where mechanistic information is missing or insufficient (eg in FBA)} 

Functional thinking -- as opposed to describing cells mechanistically, as physical objects -- is fundamental to biology. In biology, the notion of "function" is justified by the fact that organisms emerged from evolution -- that is, as a result of mutation and selection -- where completely "nonfunctional" solutions are probably being selected against. Evolution itself is an open-ended process and does not entail any simple criterion for "optimality". Since selection depends on changing environments, and since environments themselves can be shaped by organisms, there is no simple, general criterion for Darwinian fitness (except for the fact, post hoc, that a species managed to survive over a long period of time). However, if we look at the end result -- an evolved species, or an evolved trait in microbes, for example, how cells allocate their resources -- and assume that this species evolved in a constant environment, it is tempting (and, as we argue here, meaningful) to describe this result by optimality approaches or economic thinking. \comment{Also mention questions in biotech?} Hence, it is not by chance that some cell models bear strong resemblances with economic models. 

\comment{Diana: "economic models": as someone who knows nothing about economic models, I would find it helpful to have a short explanation of what we mean. Like, are economic models always about optimality or is that just one part of economy that we picked because it is potentially useful?}

\comment{wolf: edit this sentence:} Since bringing systems biology and economics together is overdue and there are no books focused on the intersection of these two fields, although there are books on systems biology from an 'economics' perspective, we decided to write a textbook covering basic knowledge about production processes in cells, their regulation, and their description in terms of resource allocation or costs and benefits. 

\comment{say a few words about the scope (topics, specific perspective) of the book}

\subsection*{Who is this book for?}

The book focuses on the application of economic principles to cell biology, providing readers with a quantitative framework to understand how cells allocate resources, optimize processes, and make trade-offs.  The topic of this book has emerged from the field of systems biology, and accordingly, we address students and researchers in related fields with a background in biology, physics, engineering, or math who want to explore this interdisciplinary field. For students, the textbook offers a structured introduction to the economic principles that govern cellular behavior, starting with basic concepts and advancing to more complex models. For researchers, it provides an overview of the current literature, helping those in related fields quickly grasp key ideas and approaches in this area of study. \comment{briefly refer to figure \ref{OVE:fig:Pre}} For readers without a biological background, we recommend the book “Cell biology by the numbers” (\href{http://book.bionumbers.org/}{book.bionumbers.org}), which takes the reader on a journey through various aspects of cell biology.

\comment{write something more specific for readers coming from different disciplines?}

Our aim is to make this book accessible to as many people as possible by ensuring that the concepts are accessible to everyone, covering both beginner and advanced topics, and by offering it as a free resource. The book and its individual chapters can be downloaded from our website \url{https://principlescellphysiology.org/book-economic-principles/}. A new version is released every three months and since the project is still ongoing, the text will be improved edition by edition.

\section*{A guide to the book}
\addcontentsline{toc}{section}{A guide to the book}  

\subsection*{Chapters overview}

A main topic of this book is resource allocation in cells. Focusing on metabolism, we can ask, more specifically, about (potentially optimal) configurations of fluxes, protein concentrations, and metabolite concentrations. This question may be given in a simplified form, e.g.~as a choice of fluxes under constraints (in Flux Balance Analysis models) or an allocation of a finite protein budget to cellular tasks (as in whole-cell models). But the overall aim behind this is to describe an entire growing cell. If we simplify this again by looking at parts of a cell (e.g.~considering small-molecule metabolism only) or looking at "low resolution" (i.e.~considering only a few global variables), this leads to different modeling approaches which we explore in this book.

\begin{figure}[t]
  \begin{center}
  \includegraphics[width=0.6\columnwidth]{\chapterroot images/OVE_Pre.pdf}
  \end{center}
  \caption{Background knowledge and modeling in this book -- Many topics in this book are presented via mathematical models. Models can capture and structure knowledge from biology, chemistry and physics in a mathematical formulation. As a “simplified replica” of reality, they highlight certain aspects of cells that we would like to describe and make them amenable to analysis. In the book, most models either describe cell metabolism (as a whole, or parts of it) or a growing cell as a whole. Aside from the basic description of steady states (in metabolism) or steady growth states (of cells), the book captures some advanced topics related to cell behavior in time, in cell communities, in uncertain environments, or aspects of spatial structure.}
  \label{OVE:fig:Pre}
\end{figure}

The reader will learn how economic principles such as optimization, resource allocation, and trade-offs can be applied to cellular biology. The chapters are organized to guide the reader from basic concepts to more advanced applications. The book covers foundational topics first and then progresses to more specialized areas, including how to develop and analyze models that explain how cells manage resources and optimize their internal processes. By the end of the book, the readers will have a solid understanding of how economic principles can be used to analyze and model cellular behavior. 

\begin{figure}[t]
  \begin{center}
  \includegraphics[width=0.45\columnwidth]{\chapterroot images/OVE_TreeNum.pdf}
  \end{center}
  \caption{Topics of the book shown as the branches of a tree, and book chapters shown as fruit -- The first chapters provide background knowledge, represented by the roots of the tree. In the following chapters we explain different modeling approaches that focus on different aspects, represented by the tree's trunk. In the chapters on metabolism and on cell models, we assume steady (growth) states and move step by step towards more complex models (resource allocation in cells), shown as the first line of branches of the tree. Finally, we consider more specific aspects such as time, variability, and space, as higher branches of our tree. The numbers in the figure indicate chapters (for chapter titles see text).}
  \label{OVE:fig:Tree}
\end{figure}

The book chapters are related to a number of larger topics, as shown in Figure \ref{OVE:fig:Tree}. 

\begin{enumerate}
    \item \textbf{The functioning of cells} - After the introductory chapter \chapterref{FAC}, “The cell as a factory”, you will find two chapters with background information about cells and their metabolism. Chapter \chapterref{CEN}, “An inventory of cell components” describes the main components of a cell, their functions, and their typical abundances in a cell. In a self-replicating cell, these are the components that need to be reproduced while also acting as the "materials" and "machines" that make reproduction possible. Chapter \chapterref{MET}, “Cell metabolism”, focuses on metabolic reactions and pathways and shows how chemical conversions depend on enzyme kinetics and reaction thermodynamics. Readers familiar with cell biology and metabolic models may skip these two chapters.\\
    \item \textbf{Metabolism} - The following four chapters concern metabolic models, starting with  models focusing only on metabolic fluxes (chapters \chapterref{CBM}, “Metabolic flux distributions”, and \chapterref{FLX}, “Optimization of metabolic fluxes”) and then continuing with models that consider enzyme kinetic rate laws to link metabolic production to enzyme demand. Chapter \chapterref{PAT}, “The enzyme cost of metabolic fluxes” assumes that (desired) metabolic fluxes are given and asks how much enzyme is needed to support them, and how metabolite concentrations should be chosen to minimize this enzyme demand. Chapter \chapterref{OME}, “Optimization of metabolic states”, combines these aspects and presents a general way to determine optimal metabolic fluxes, metabolite concentrations, and enzyme levels at the same time. At the end of these chapters, you will have learned what arrangements of fluxes and concentrations make cell metabolism maximally efficient, that is, allowing to produce a maximal amount of product at a limited enzyme capacity.\\
    \item \textbf{Cell models} - In the following two chapters, we consider the cell as a whole. Chapter \chapterref{SMA}, “Principles of cell growth”, describes what a system, the cell, needs to do in order to replicate, and what internal arrangements will lead to a maximal growth rate. Chapter \chapterref{LAR}, “Resource allocation in complex cell models”, shows how these general principles are applied in large cell models that describe small-molecule and macromolecule metabolism at a great level of detail.\\
    \item \textbf{Time and uncertainty} - While the models in the previous chapters all assumed steady states, and often a simple choice of the “best state” for a cell, the following chapters explore some more possibilities and how one can describe them by models. Chapter \chapterref{TIM}, “Optimal cell behavior in time”, extends the question of optimal resource allocation to optimal scheduling processes in time, where the cell needs to achieve its goal in a certain time horizon and resources can be shifted between different moments in time. The next two chapters are concerned with variability. Chapter \chapterref{VAR}, “Diversity of metabolic fluxes in a cell population”, explores how cells in a population, instead of realizing the same optimal flux distribution, may realize different fluxes, creating random differences between individual cells in a given environment. On the contrary, Chapter \chapterref{UNC}, “Cells in the face of uncertainty” assumes that cells live in an unpredictable environment and need to “make bets” on how the environment will change in the future, and addresses what are the best strategies.\\
    \item \textbf{Sizes and shapes} - The last two chapters of the book are concerned with space in a broader sense. Chapter \chapterref{CDC}, “Strategies for cell size control” describes how cells choose the moment of cell division, which determines the distribution of cell sizes in cell populations. Chapter \chapterref{ORG}, on the “Economy of organ form and function”, goes beyond microbiology and describes more broadly how systems in the body and their physiological usage -- in this case, the lungs in mammals and the speed and depth of respiration -- are shaped by their size, and how general scaling laws for shapes can give rise to similar laws for biological function and the “economics” of the system in question.
\end{enumerate}

\subsection*{A few words on mathematical models}

\begin{figure}[t]
\begin{center}
\begin{tabular}{ll}
(A) & (B)\\[2mm]
  \includegraphics[width=0.5\columnwidth]{\chapterroot images/OVE_MetModelsLevelsOfDescription_a.pdf}\hspace{0.1\textwidth}
  &   \includegraphics[width=0.25\columnwidth]{\chapterroot images/OVE_MetModelsLevelsOfDescription_b.pdf}
\end{tabular}
  \end{center}
  \caption{Metabolic models and levels of description -- (A) Main elements of metabolic models. The example shows a linear pathway of 3 enzyme-catalyzed reactions. Following the convention in kinetic models, the “boundaries” of the model are external metabolites (marked as "ext"). In flux analysis models, the boundaries are typically not formed by metabolites, but by exchange reactions. (B) Levels of description of a metabolic model, from network structure (metabolites and reactions) to a quantitative physical description (comprising for example concentrations and fluxes) and further to a function-related “economic” description (comprising physiological constraints, costs, and benefits).}
  \label{OVE:fig:MetModelsLevelsOfDescription}
\end{figure}

As mentioned in Figure \ref{OVE:fig:Pre}, in this book, cells and cell behavior will be largely described with the help of mathematical models, often used in biology to gain insight into biological systems through simulations and quantitative analysis. \comment{(Give 1 or 2 examples if you like, but I don't think it is necessary here)} \comment{maybe also add some sentences for readers who are not so familiar with mathematical models, saying that this is very common in physics, and what is their purpose (also refer to box “Qualities of a model”)} As shown in Figure   \ref{OVE:fig:MetModelsLevelsOfDescription}, our models typically describe a set of metabolites and the reactions that convert them, forming a network; we then attribute concentrations to the metabolites and chemical fluxes to the reactions and describe their dynamics; and finally, based on this dynamics, we consider "economic" questions, often in the form of optimality problems. Although different chapters will focus on different types of models (describing metabolic fluxes, compound concentrations, cell growth, or all of these aspects together) and models of different size (from simple instructive 3-variable models to models covering thousands of different cell components), all these models eventually describe different aspects of one cell and the same cell. Therefore, the different types of model are closely related and sometimes one model can be seen as a simplified form of another one. Figure \ref{OVE:fig:CellScheme} shows a basic scheme of a cell, where precursors produced in metabolism are converted into proteins, which then constitute the machines that catalyze metabolic reactions (as enzymes) or protein production (as ribosomes). By “zooming in” and focusing on different aspects of this scheme, we obtain the main types of models that we will encounter in this book.

\begin{figure}[t!]
    \begin{center}
  \includegraphics[width=1.0\columnwidth]{\chapterroot images/OVE_CellScheme.pdf}
  \end{center}
  \caption{Simple scheme of a cell, and common function-based (“economic”) cell models --  A microbial cell, depicted here by a very simplified scheme (center top), can be viewed in various ways. Two main views on cells come from quantitative data obtained from experiments (here represented by the proteome, top right) and from the network of metabolic reactions (left), covering all (or a part) of the production and conversion processes in the cell. These conceptual pictures can be translated into mathematical models that describe (and predict) a number of cell variables.  The three remaining boxes refer to three common types of resource allocation models presented in this book, each covering a different scope - from metabolism to entire cells. The formulae are explained in later chapters ($\mathbf{N}$: stoichiometric matrix; $\mathbf{v}$: vector of fluxes; $b(\mathbf{v})$: flux benefit function; $a(\mathbf{v})$: flux cost function; $k_{\rm app}$: apparent catalytic rate of an enzyme; $v(e,\mathbf{c})$: rate law of an enzymatic reaction, giving the rate as a function of enzyme level $e$ and metabolite concentration vector $\mathbf{c}$).}
  \label{OVE:fig:CellScheme}
\end{figure}

\subsection*{Where to find more information}

In addition to the main text, the book offers additional material.

\paragraph{Background knowledge and literature.} 
Cellular economics - and systems biology more generally - builds on knowledge from different disciplines and on a history of ideas in biology and beyond. In the section "Reading recommendations" at the end of the book, you will find a number of books, articles, and online resources that provide background information.

\paragraph{Reading recommendations for individual chapters.} 
For readings specific to individual chapters, please see the "Recommended readings" sections at the end of each chapter.

\paragraph{Boxes.} 
In the chapters, some specialized topics or thoughts on the side can be found in separate boxes. Most of these boxes belong to one of these categories: Economic analogies, Philosophical remarks, Physical thoughts and analogies, Mathematical details, Experimental methods in biology. The remaining boxes contain ideas that did not fit into this simple scheme. A list of all the boxes can be found at the end of the book.

\paragraph{Book website.}
More information about the book and the economic cell collective behind it can be found on our website \url{https://principlescellphysiology.org/book-economic-principles/}.

\paragraph{Problems and computer exercises.}
The problems at the end of each chapter are a mix of conceptual questions, paper-and-pencil calculation exercises, and computer exercises. Solutions to some of the problems can be found in the end of the book. More computer exercises (Jupyter notebooks) can be found on our website: \url{https://principlescellphysiology.org/book-economic-principles/problems.html}.
    
\paragraph{Lectures.}
All book chapters have been presented as lectures at our "Economic Principles in Cell Biology" summer schools at LPI Paris. Lecture slides are provided on our website: \url{https://principlescellphysiology.org/book-economic-principles/lectures.html}.
   
\paragraph{You can participate in writing this book.}
You can participate in our project in many ways. If you have direct feedback for us (which may concern anything from typos to proposing new topics), please let us know via our feedback form on the website. If you would like to be directly involved (in writing, reviewing, proofreading, graphics design, or any other smaller or larger tasks), please contact us anytime. For more information, see \url{https://principlescellphysiology.org/book-economic-principles/contribute/EPCB.html}.

\comment{PLEASE NOTE: The "reading recommendations" section has been moved to a separate file in the main book git.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       please do not edit this block                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifx\weAreInMain\undefined                                                      %
\bibliographystyle{unsrtnat}                                                    %
%\bibliography{\chapterroot bibliography-chapter}                                %
\end{document}                                                                  %
\fi                                                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 14:28:35 2018

@author: jew
"""

from scipy.optimize import fmin
from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.optimize import minimize
from scipy import stats
   
import matplotlib
from cycler import cycler
#cc = cycler(color=['#0072b5','#bc3c28','#e18726','#1f854e','#7776b1']) # NEJM
cc = cycler(color=['#374e55','#df8f44','#00a1d5','#b24746','#79af97','#f39b7f']) # JAMA
matplotlib.rcParams.update({'font.size': 8}) # 8 for thesis
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['axes.prop_cycle'] = (cc)

v = []

for fname in ['discretization_times','c','pr','pm','ur']:
    with open(fname + '.export') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    v.append(np.array(content,dtype=float))
    
cut = 3800

t = v[0][0:cut]; p = v[1][0:cut]; r = v[2][0:cut]; m = v[3][0:cut]
q = 1 - r - m; ur = v[4][0:cut]
t0 = t[0]; t1 = t[-1]
umax = 0.6

names = ['c','$p_R$','$p_M$','$u_R$']

plt.figure(0, figsize=(4,1.5))
ax = plt.gca()
plt.xlabel('Time [h]')
plt.ylabel('$u_R$')
plt.plot([t0,t1],[0,0],linewidth=1,color='grey',linestyle='dotted')
plt.plot([t0,t1],[umax,umax],linewidth=1,color='grey',linestyle='dotted')
plt.plot(t,ur*umax,linewidth=1,label='$u_R$')
ax.set_xlim(t0,t1)
#ax.set_ylim(0,1)
plt.savefig('C.pdf', bbox_inches='tight', format='pdf', dpi=1000)

plt.figure(1, figsize=(4,1.5))
plt.xlabel('Time [h]')
plt.ylabel('Mass fractions')
ax = plt.gca()
# ax2 = ax.twinx()
# ax2.set_ylim(ax.get_ylim())
#plt.tick_params(axis='y', which='minor', labelleft='off', labelright='on')
#plt.yticks([rmax], ['$r_{max}$'])
ax.set_xlim(t[0],t[-1])
ax.set_ylim(0,1)
plt.fill_between(t, r, label='$p_R$', color='#00a1d5', alpha=1)
plt.fill_between(t, r, r+m, label='$p_M$', color='#b24745', alpha=0.8)
plt.fill_between(t, r+m, 1, label='$p_Q$', color='#374e55', alpha=0.3)
#plt.fill_between(t, q, np.ones_like(t), color='#0f9d58', label='$w$')
#plt.plot(t, r, color='#374e55',linewidth=0.5)
#plt.plot(t, r+m, color='#b24746',linewidth=0.5)
#plt.grid(linestyle='dotted',color='black')
plt.legend(loc = 2)
plt.savefig('Dv1.pdf', bbox_inches='tight', format='pdf', dpi=1000)

# plt.figure(2, figsize=(3,2))
# plt.xlabel('Time [h]')
# plt.ylabel('$u_R$')
# ax = plt.gca()
# ax.set_xlim(t[0],t[-1])
# ax.set_ylim(0,1)
# plt.plot(t, r, color='#374e55',linewidth=1)
# plt.savefig('Dv2.pdf', bbox_inches='tight', format='pdf', dpi=1000)

#plt.savefig('steady.pdf', bbox_inches='tight', format='pdf', dpi=1000)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 14:28:35 2018

@author: jew
"""

from scipy.optimize import fmin
from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.optimize import minimize
from scipy import stats
   
import matplotlib
from cycler import cycler
#cc = cycler(color=['#0072b5','#bc3c28','#e18726','#1f854e','#7776b1']) # NEJM
cc = cycler(color=['#374e55','#df8f44','#00a1d5','#b24746','#79af97','#f39b7f']) # JAMA
cc2 = cycler(color=['#df8f44','#00a1d5','#b24746','#79af97','#f39b7f']) # JAMA
matplotlib.rcParams.update({'font.size': 8}) # 8 for thesis
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['axes.prop_cycle'] = (cc)

v = []

for fname in ['discretization_times','u0','u1','u2','u3','x1','x2','x3']:
    with open(fname + '.export') as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    v.append(np.array(content,dtype=float))

t = v[0]
u0 = v[1]; u1 = v[2]; u2 = v[3]; u3 = v[4]
x1 = v[5]; x2 = v[6]; x3 = v[7]
t0 = t[0]; i2 = len(t)

# extend everything
t  = np.hstack([t,np.arange(t[-1],t[-1]+1,np.median(np.diff(t)))])
x1 = np.hstack([x1,np.ones(len(t)-len(x1))*x1[-1]])
x2 = np.hstack([x2,np.ones(len(t)-len(x2))*x2[-1]])
x3 = np.hstack([x3,np.ones(len(t)-len(x3))*x3[-1]])
u0 = np.hstack([u0,np.ones(len(t)-len(u0))*0.25])
u1 = np.hstack([u1,np.ones(len(t)-len(u1))*0.25])
u2 = np.hstack([u2,np.ones(len(t)-len(u2))*0.25])
u3 = np.hstack([u3,np.ones(len(t)-len(u3))*0.25])

i0 = np.where(np.diff(u1))[0][2]
i1 = np.where(np.diff(u2))[0][2]

t1 = t[-1]

names = ['$u_0$','$u_1$','$u_2$','$u_3$',
         '$x_1$','$x_1$','$x_2$']

plt.figure(0, figsize=(7,2.5))
plt.subplot(421)
ax = plt.gca()
plt.ylabel('$u_0$')
plt.axhline(1,linewidth=1,color='grey',linestyle='dotted')
plt.axhline(0,linewidth=1,color='grey',linestyle='dotted')
plt.axvline(t[i0], color='grey',linestyle='dotted',linewidth=1)
plt.axvline(t[i1], color='grey',linestyle='dotted',linewidth=1)
plt.axvline(t[i2], color='grey',linestyle='dotted',linewidth=1)
plt.plot(t,u0,label='$u_0$',zorder=1000)
matplotlib.pyplot.xticks(color='w')
ax.set_xlim(t0,t1)

ax2 = ax.twiny()
ax2.set_xlim(ax.get_xlim())
plt.tick_params(axis='x', which='minor', labelbottom='off', labeltop='on')
plt.xticks([t[i0],t[i1],t[i2]], ['$t_0$','$t_1$','$t_f$'])

plt.subplot(423)
ax = plt.gca()
plt.ylabel('$u_1$')
plt.axhline(1,linewidth=1,color='grey',linestyle='dotted')
plt.axhline(0,linewidth=1,color='grey',linestyle='dotted')
plt.axvline(t[i0], color='grey',linestyle='dotted',linewidth=1)
plt.axvline(t[i1], color='grey',linestyle='dotted',linewidth=1)
plt.axvline(t[i2], color='grey',linestyle='dotted',linewidth=1)
plt.plot(t,u1,label='$u_1$',zorder=1000)
matplotlib.pyplot.xticks(color='w')
ax.set_xlim(t0,t1)

plt.subplot(425)
ax = plt.gca()
plt.ylabel('$u_2$')
plt.axhline(1,linewidth=1,color='grey',linestyle='dotted')
plt.axhline(0,linewidth=1,color='grey',linestyle='dotted')
plt.axvline(t[i0], color='grey',linestyle='dotted',linewidth=1)
plt.axvline(t[i1], color='grey',linestyle='dotted',linewidth=1)
plt.axvline(t[i2], color='grey',linestyle='dotted',linewidth=1)
plt.plot(t,u2,label='$u_2$',zorder=1000)
matplotlib.pyplot.xticks(color='w')
ax.set_xlim(t0,t1)

plt.subplot(427)
ax = plt.gca()
plt.xlabel('Time [h]')
plt.ylabel('$u_3$')
plt.axhline(1,linewidth=1,color='grey',linestyle='dotted')
plt.axhline(0,linewidth=1,color='grey',linestyle='dotted')
plt.axvline(t[i0], color='grey',linestyle='dotted',linewidth=1)
plt.axvline(t[i1], color='grey',linestyle='dotted',linewidth=1)
plt.axvline(t[i2], color='grey',linestyle='dotted',linewidth=1)
plt.plot(t,u3,label='$u_3$',zorder=1000)
ax.set_xlim(t0,t1)

plt.subplots_adjust(hspace=0.2, wspace=0.3)

matplotlib.rcParams['axes.prop_cycle'] = (cc2)
plt.subplot(122)
plt.xlabel('Time [h]')
plt.ylabel('Metabolites concentration')
ax = plt.gca()
ax.set_xlim(t[0],t[-1])
#next(ax._get_lines.prop_cycler)
plt.axvline(t[i0], color='grey',linestyle='dotted',linewidth=1)
plt.axvline(t[i1], color='grey',linestyle='dotted',linewidth=1)
plt.axvline(t[i2], color='grey',linestyle='dotted',linewidth=1)
plt.plot(t, x1, label='$x_1$')
plt.plot(t, x2, label='$x_2$')
plt.plot(t, x3, label='$x_3$')
plt.legend()
ax.set_ylim(0,ax.get_ylim()[1])

ax2 = ax.twiny()
ax2.set_xlim(ax.get_xlim())
plt.tick_params(axis='x', which='minor', labelbottom='off', labeltop='on')
plt.xticks([t[i0],t[i1],t[i2]], ['$t_0$','$t_1$','$t_f$'])

plt.savefig('im_diego.pdf', bbox_inches='tight', format='pdf', dpi=1000)

v0 = 1*4/(1 + 4)*u0[-1]
v1 = 2*x1[-1]/(1 + x1[-1])*u1[-1]
v2 = 4*x2[-1]/(1 + x2[-1])*u2[-2]
v3 = 3*x3[-1]/(1 + x3[-1])*u3[-3]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       please do not edit this block                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifx\weAreInMain\undefined                                                      %
\documentclass{book}                                                            %
\def\mainfolder{./main}              %
\input{main/macros.tex}                                                         %
\input{main/style.tex}                                                          %
\usepackage{hyperref}                                                           %
\begin{document}                                                                %
  \renewcommand{\chapterroot}{./}                                               %
\fi                                                                             %
%                                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{The cell as a factory}
\label{FAC}

\hideinbooklayout{
\begin{center}
  {\large\textbf{Version: 2022-01-07}}%
\end{center}
}

\includechapterauthors{Ohad Golan}

\comment{wolf: if you like, please add an abstract and keywords (for the zenodo version)
\abstract{}
\chapterkeywords{}
}

\chapteracknowledgments{This chapter was planned and outlined by Ohad Golan with the help of Wolfram Liebermeister and Elad Noor.}

\chaptercitation{O. Golan}{The cell as a factory}{10.5281/zenodo.12592514}

The term "metabolism" is usually used to describe the chemical reactions that occur within biological organisms to produce and transform molecules needed to sustain life. Although this definition is useful, it does not give scientific or mathematical ground for the analysis of metabolic systems. Here we consider metabolic systems in a much broader sense, and in order to provide a logical framework for the analysis of metabolic systems, we begin with a more formal definition that also covers systems outside biology. 
Metabolic system: "A well-defined system that takes up nutrients and uses them to sustain itself".
This definition can be represented by a simple chemical equation:

\begin{equation*}
  \mbox{nutrients} \longrightarrow \mbox{metabolic system + waste products} 
\end{equation*}

The process is carried out by the metabolic system itself - a point we will expand on later. The waste products are typical leftovers of the reaction in case such products exist. The most obvious example of a metabolic system is a biological system that takes up substances from its environment and assimilates them to reproduce its own components (often summarized as "biomass"). The chemical equation of metabolism for biological systems is:

\begin{equation*}
  c_1~\mbox{sugar}+c_2~\mbox{oxygen}+c_3~\mbox{ammonia} \longrightarrow \mbox{biomass + waste products} 
\end{equation*}

The equation describes all the nutrients, including sugar, oxygen, and ammonia, that are necessary to sustain a biological system. Other molecules such as certain metals and phosphate are also necessary for the reaction to occur, but we neglect them for the sake of brevity. The typical waste products are water, carbon dioxide, and other possible chemicals secreted by the system.

\begin{Ecoblock}[float=t,title={A cell and a construction firm as black boxes}]
\begin{center}
\includegraphics[width=0.4\textwidth]{\chapterroot /images/FAC_BlackBox.pdf}\\
\label {FAC:fig:BlackBox}
\end{center}

Another, less typical, example of a metabolic system in this general sense is an economic firm supplying a product. In this example we will consider for simplicity a firm that builds houses, but any kind of product can be equally used.
Such a firm takes in land and different construction materials, these would be equivalent to the nutrients, and by the use of the labor force, which would be equivalent to the proteins, uses them to build houses. The houses are then sold to maintain and increase the value of the firm, just as the biological cell maintains itself. The chemical equation of metabolism for a construction firm is:
\begin{equation*}
  c_1~\mbox{land} + c_2~\mbox{construction materials} \longrightarrow \mbox{value} + c_3~\mbox{waste} 
\end{equation*}
%\begin{minipage}{\columnwidth}{\centering}
%\end{minipage}
\end{Ecoblock}

In this book we focus on the analysis of biological metabolic systems. However, given that economic systems fall under the same definition of a metabolic system, we will use them as analogies to simplify explanations. Whenever an analogy to economical systems is presented in this book, it will be displayed in an "Economic analogy" box such as the one above.

Many metabolic systems use a strategy of reproduction to sustain itself. That is, nutrients are used to make more of the metabolic system and not only to maintain it. This means that the output of the metabolic process is more of the metabolic system. This creates a system that, when unlimited resources are available, grows exponentially - the metabolic system takes in nutrients which it uses to replicate, the output of the process is also the metabolic system which takes in more nutrients and also replicates. 
Metabolism includes all the processes that take place in order to carry out the overall chemical conversion of metabolism - that is, everything that happens inside the black box described above. The most fundamental model of a metabolic system is one that takes nutrients from the environment, breaks them down into building blocks, and uses these building blocks to sustain itself. In biological systems, these processes are termed catabolism and anabolism. In catabolism, the cell takes up carbon and nitrogen sources from the environment and uses them to synthesize the necessary building blocks: amino acids, nucleic acids, and fatty acids. In the anabolic process, the building blocks are used to form biomass which includes the functioning systems of the cells, proteins, DNA strands, and the membrane. Each process is catalyzed by a specific set of enzymes. These enzymes that catalyze the reactions are actually the metabolic system itself. When the cell grows, it makes more enzymes to catalyze more reactions - this is the reproduction process that leads to exponential growth.

\begin{Ecoblock}[float=t, title={A cell to a construction firm as growing systems}]

In an analogy to an economic system of a construction firm, the catabolic process would correspond to the purchase and transfer of the construction materials to the construction site, and the anabolic processes would correspond to the construction of the house; the catalytic enzymes would correspond to the workers carrying out the transfer of the materials and construction process. The growth process in bacteria is analogous to the growth of the firm - when the construction of the house is complete, the house is then sold  to increase the value of the firm. The increased value enables the company to hire more workers and build more houses.

\begin{center}
   \includegraphics[width=0.55\textwidth]{\chapterroot /images/FAC_FactoryAnalogy.pdf}\\
   \label {FAC:fig:FactoryAnalogy}
\end{center}

\end{Ecoblock}

The metabolic system controls the allocation of the available resources. When coordinating the process, the metabolic system decides between different strategies on how to best use the resources. For example, the cell decides how much of the available enzymes to allocate to the catabolic process and how much to the anabolic process. When making these decisions, the cell takes into account different physical constraints. Examples of these physical constraints are: a limited physical volume to maintain and carry out the metabolic processes, a limited surface area that constrains the ability to take up nutrients, or limiting thermodynamic constraints on the activity of the enzymes. There is no one best strategy that is always utilized - different organisms decide on different strategies based on the living conditions. This decision process is carried out by many mechanisms in the cell, the main information processing core of the cell being DNA. The decisions carried out by the cell are based on the evolutionary process the metabolic system has gone through during its existence. A description of cell information processing and how it is carried out is given in Appendix A. 

\begin{Ecoblock}[float=b,title={Allocation of workforce}]
In an analogy to the economic system of a construction company, the company manager faces the decision of how to allocate his workforce, how many of his workers to assign to bring in materials from the factory and how many of his workers to assign to the construction process. In a similar way to the biological system, there are different limiting constraints, such as a difficult topographic construction site or limited available resources. Unlike the biological cell, though, in which the decision-making is embedded by the evolutionary processes, here the decision is made by the manager of the construction site.
\end{Ecoblock}

So far, we describe the most fundamental metabolic system. This is a coarse-grained description in which the cell catabolizes nutrients into one type of precursor and does not take into account all the processes that take place in catabolism and anabolism. In a biological metabolic system, the cell requires multiple different types of precursors, such as amino acids, nucleic acids, and fatty acids. To create all the different precursors, the cell takes in nutrients from the environment and, through a set of chemical reactions, turns the nutrients into the precursors that are necessary for the cell to sustain itself. Each chemical reaction in the metabolic process is carried out by proteins. The different precursors can be produced through different sets of chemical reactions known as metabolic pathways, and the different chemicals in the metabolic pathways are known as metabolites. The cell decides which metabolic pathway to activate by producing the necessary enzymes. In an analogy to the economic system of a construction company, each chemical reaction is one process carried out by a worker – for example, the assembly of the frame of the house requires a carpenter, while the next step in the construction pathway is to place the foundation in the correct location, which is done by another worker. The workers are analogous to enzymes, and the different parts necessary for construction are the metabolites.

Many metabolic pathways have overlapping metabolite reactants and products. Some of the key parameters that describe metabolic pathways are the enzyme catalytic rates. These parameters describe the rate at which the enzymes consume and produce metabolites and at which concentration of reactants they saturate. In the analogy to the construction firm, the enzymatic parameters are parameters that describe the rate of work of each worker. Given that each metabolic pathway is made up of a series of chemical reactions, each with different catalytic rates, the different enzymes of each pathway must be coordinated perfectly to avoid any excess buildup of metabolites - just like in a factory assembly line, all the workers must be coordinated together to avoid buildup of an intermediate.

\begin{figure}[t!]
  \centerline{
   \includegraphics[width=0.7\textwidth]{\chapterroot /images/FAC_Intermidiates.pdf}\\[2mm]
 }
  \caption{A self-replicating cell -- In the metabolic process the cell takes in available nutrients and through a set of biochemical reactions, turns them into precursors necessary for growth. The chemical reactions are carried out by proteins in the cell.   }
   \label {FAC:fig:Intermidiates}
\end{figure}

In order to make sense of the complex network of metabolic reactions, different mathematical models were developed. The models take into account the known experimental data for the different reactions and compile them together to predict the overall response of the system under different growth conditions. 

The metabolic models described above describe biological systems that are disconnected from the environment except for some artificial supply of nutrients. In natural ecological systems, different organisms exist together under a limited supply of nutrients. They compete or cooperate to best utilize the limited available resources. All organisms try to improve their chances of survival according to the laws of evolution. In such a setting, the metabolism of organisms living in an ecological system is directly dependent on the other organisms that co-exist with them. In an analogy to an economic system, this would be a competition between different companies for the same possible clientele. Some companies would compete against each other, while others would cooperate to improve their profit. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       please do not edit this block                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifx\weAreInMain\undefined                                                      %
\end{document}                                                                  %
\fi                                                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
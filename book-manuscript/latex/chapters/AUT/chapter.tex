%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    please do not edit this block   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifx\weAreInMain\undefined           %
\documentclass[a4paper]{book}        %
\def\mainfolder{./main}              %
\IfFileExists{main/macros.tex}       %
  {\input{main/macros.tex}}          %
  {\input{../../main/macros.tex}}    %
\IfFileExists{main/style.tex}        %
  {\input{main/style.tex}}           %
  {\input{../../main/style.tex}}     %
\input{./macros-chapter.tex}         %    
                                     %
\begin{document}                     %
  \renewcommand{\chapterroot}{./}    %
\fi                                  %
%                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\chapter{The von Neumann-Leontief framework of autocatalytic chemical networks}
%\chapter{Resource limitation based approaches for autocatalytic networks}
\label{AUT}

%\comment{\begin{center}{\large\textbf{Version: \today}} \end{center}}

\includechapterauthors{
  David Lacoste\affiliationnum{1}\orcid{0000-0002-5735-4657},
  Barnabé Ledoux\affiliationnum{1}\orcid{0009-0005-1072-5886}
}
\affiliations{$^1$ Gulliver Laboratory, ESPCI, 10 rue Vauquelin, Paris, France}
%\comment{In the authors list, put editors and main authors in alphabetical order (by last name); please add affiliation numbers and ORCIDs as shown.}

\abstract{Autocatalytic networks are prevalent in nature, from the metabolism of single cells to the food webs of ecosystems. Here, we address a number of essential points about them : (i) how to identify them within complex chemical reaction networks using only considerations of stoichiometry (ii) how to model them using ideas introduced by Von Neumann and Leontief in the context of economy and ecology. We discuss some applications of such a framework for modeling cell metabolism.}

\chapterkeywords{autocatalysis, metabolism, cell growth, linear programming, production function}

\chapteracknowledgments{This book chapter is mainly based on these two recent articles : Anjan Roy et al., A unifying autocatalytic network-based framework
for bacterial growth laws, Proc. Natl. Acad. Sci. U.S.A. 118(33):e2107829118 (2021) and A. Blokhuis et al., Universal motifs and the diversity of autocatalytic systems, Proc. Natl. Acad. Sci. U.S.A., 117, 25230 (2020). We thank R. Pugatch for comments.}

\chaptercitation{D.~Lacoste and B.~Ledoux}{Universal features of autocatalytic systems}{PUT-THE-DOI-HERE}

%
\begin{chapterhighlights}

\begin{itemize}
   \item A stoichiometric theory of autocatalysis is presented, which is based on the notion of productivity (either economical or chemical). Application of this theory to the identification of autocatalytic subnetworks is outlined. This notion of stoichiometric autocatalysis is contrasted with the dynamical notion of autocatalysis.
  \item The expanding economic model of von Neumann model is presented, which is also based on the same notion of productivity.  
\item An important special case of the linear von Neumann model is the Leontief model. The Leontief model  can be used to model cell metabolism. We present briefly two applications: the first one concerns the derivation of growth laws and second one the prediction of the inhibition of bacterial growth by various types antibiotics.
 
  
\end{itemize}
\end{chapterhighlights}
%

\section{Introduction}
\label{AUT:section:Introduction}



\section{Stoichiometric vs. dynamical autocatalysis}
\label{AUT:section:Structural}

%Structural properties are properties that hold independently of the value of some parameters and which have for this reason a certain degree of universality. For instance, in the context of chemical reaction networks, certain properties of the network hold independently of the values taken by kinetic parameters, only the stoichiometry matters. 

\subsection{Stoichiometric autocatalysis}
According to IUPAC, "an autocatalytic reaction is a chemical reaction in which a product (or a reaction intermediate) also functions as a catalyst. In such a reaction, the observed rate of reaction is often found to increase with time from its initial value". 
While this definition provides a sound kinetic characterization of autocatalysis, it is not easy to use it to identify autocatalysis in situations in which kinetics is poorly known. Such situations arise frequently when trying to analyze complex chemical mixtures, such as astrophysical samples analyzed by researchers studying the origin of life research, or man-made prebiotic systems such as complex interacting RNA networks, in which most of the species and reactions are unknown. 

The analysis of this kind of system calls for a new definition of autocatalysis based on stoichiometry rather than kinetics. Such a definition of autocatalysis has been proposed in \citeauthor{BlokhuisLacoste2020} building on Gordan's theorem of linear algebra, which is mentioned in the box \ref{AUT:box:Gordan}. 
For alternate definitions of autocatalysis see in particular \citeauthor{AndersenFlamm2021}.
In simple terms, this stoichiometric definition of autocatalysis relates autocatalysis to the existence of a subnetwork devoid of mass-like conservation laws. More precisely, autocatalysis requires the existence of a submatrix $\mathbf{S}$ of a larger stoichiometric matrix $\mathbf{\nabla}$ and a non-zero reaction vector $\mathbf{x}$, such that $\mathbf{S}$ is \textit{non-ambiguous}, \textit{autonomous} and 
\begin{equation}
\label{AUT:eq:def_autocat}
\Delta \mathbf{n}=\mathbf{S} \cdot \mathbf{x}>0.
\end{equation}
The existence of the vector $\mathbf{x}$ guarantees that there is a set of species and reactions such that all species of the set are produced by the autocatalytic subnetwork, in other words Eq. \ref{AUT:eq:def_autocat} guarantees that the network is \textit{productive}.

The other important notions are :
\textit{Autonomy} (i) which means that each reaction should have at least one product and one reactant, 
and \textit{non-ambiguity} (ii) which means that a species can not be both reactant and product of the same reaction. 
The condition (i) excludes direct injection or loss of species from the environment within the set of autocatalysts, while condition (ii) is less essential but is convenient as it ensures that catalytic steps can be distinguished at the level of stoichiometric matrix \citeauthor{UnterbergerNghe2022}. 
\begin{MathDetailblock}[title={Gordan's theorem},float=t!,label={AUT:box:Gordan}]
Gordan's theorem is the following result of linear algebra which takes the form of an alternative :
\begin{equation}
\label{AUT:eq:Gordan_1}
    \exists \mathbf{x} \quad \rm{s.t.} \quad \mathbf{S} \cdot \mathbf{x} >0, 
\end{equation}
    or
\begin{equation}
\label{AUT:eq:Gordan_2}
   \exists \rho>0 \quad \rm{s.t.} \quad \mathbf{S}^t \cdot \rho =0. 
\end{equation}
The first side of the alternative in \ref{AUT:eq:Gordan_1} corresponds to the stoichiometric definition of autocatalysis, the second side of the alternative in \ref{AUT:eq:Gordan_2} corresponds to the existence of a so called mass-like mass conservation law, i.e. a conservation law with only strictly positive entries. Note that when autocatalysis is present, only mass-like conservation are forbidden but not general conservation laws in which the entries of $\rho$ are positive and negative. For a discussion of such an example see for instance \cite{KamimuraSughiyama2024}. Note also that a weaker condition for autocatalysis is the absence of any conservation law, which mathematically means $\ker \mathbf{S}^t=0$. 
\end{MathDetailblock}

Remarkably, this mathematical definition is enough to guarantee the existence of a small number of minimal autocatalytic motifs called autocatalytic cores.
The minimality of these cores means that they can not contain smaller cores in them.
In \citeauthor{BlokhuisLacoste2020}, it was found that with the assumptions which were used, only five minimal motifs could exist, which are represented in Fig. \ref{AUT:fig:motifs}.
\begin{figure}[t!]
  \begin{center}
 \includegraphics[width=0.7\textwidth]{\chapterroot/images/motifs.pdf}
   \caption{Five minimal motifs (figure taken from \citeauthor{BlokhuisLacoste2020}). Orange squares indicate where further reactions may be included provided this preserves the motif type.}
   \label{AUT:fig:motifs}
  \end{center}
\end{figure}

An important example of such autocatalytic core can be found in the work of Von Neumann who introduced  the idea of an universal constructor $U$, a machine that would be able to construct any object including itself when given some set of instructions $I$. To allow the machine to self-replicate and  evolve and also to solve a recursion problem linked to the machine itself, Von Neumann understood the need of an additional player in addition to $U$ and $I$, namely a universal copy machine $C$ that would copy the instructions without translating them \cite{SuckjoonFangwei2018}.  Thanks to this remarkable insight, Von Neumann foresaw the essential players in the translation-transcription machinery of DNA that we know today. 
The universal constructor $U$, the universal copy machine $X$ and the instructions $I$ together make a type II autocatalytic cycle, which can be summarized as the following chemical network :
\begin{gather}
     \ce{X  + I  ->2I +X }, \hspace{1em}  \\
    \ce{U + I  ->2U + I}, \hspace{1em}  \\
    \ce{B + I  -> B + I + X}.
\end{gather}


Another advantage of the stoichiometric definition of autocatalysis is that with it, linear programming algorithms, a classic tool for analyzing economic and optimization problems, can be implemented to search effectively for autocatalytic subnetworks within large chemical networks \citeauthor{PengLinderoth2022,GagraniBlanco2024}. 
In carrying out this program, the authors of  \citeauthor{GagraniBlanco2024} found that the number of autocatalytic subnetworks typically grow linearly with system size. In practice, a much smaller number of subnetworks is expected to be relevant dynamically in large networks. One can thus speculate that either many autocatalytic subnetworks are not are kinetically realizable or that a selection takes place among them, we now further discuss this issue.

\subsection{Dynamical autocatalysis}
\label{AUT:section:Dynamics}

 Recently, the connection between the topology of the chemical network and its dynamical stability has been explored in two separate works that adress different sides of that issue. In the first one, it was proven that for dilute systems with no degradation, the stoichiometric definition of autocatalysis leads to dynamical autocatalysis, characterized by a strictly positive Lyapunov exponent \citeauthor{UnterbergerNghe2022}. In the second one, for a certain class of parameter-rich kinetics, it was shown that the stoichiometric definition of autocatalysis implies a choice of reaction rates for which you have an unstable fixed point \cite{VassenaStadler2024}. The first work gives explicit results but is limited to the diluted regime, while the second work does not have this limitation, but is only an existence proof, it does provide an explicit method to obtain the reaction rate mentioned in the result.

Other recent studies have explored the connection between autocatalysis and non-equilibrium thermodynamics. 
In particular, the group T. Kobayashi, has built a chemical thermodynamic theory of open systems which are also self-replicating. This approach clarifies the thermodynamic conditions under which growth is possible in a system in which the volume is also growing \cite{KamimuraSughiyama2024}. To include this change of volume, these researchers developed an extension of traditional chemical thermodynamics theory. This change of volume is an important aspect of autocatalysis, which manifest itself in certain experiments such as the ones of Ref. \cite{LuBlokhuis2024}. In this work, 
small-molecule autocatalytic reactions occur in compartments made of droplets, which demonstrates that autocatalysis can drive compartment growth, competition and reproduction.

The question of how to connect stoichiometric, kinetic and thermodynamic features of autocatalysis is an ongoing active area of research both theoretically \cite{KoscKuperberg2024,DesponsDecker2024,KamimuraSughiyama2024} and experimentally \cite{LuBlokhuis2024}.




\section{Von Neumann's model of an expanding economy}
\label{AUT:section:Von Neumann}

In 1945, J. von Neumann made another essential to our topic in his work on an expanding economy, in which he also introduced the notion of dynamic economic equilibrium. This is a model for a circular economy, in which products (or goods) are to be produced from other goods using a number of processes with a certain intensity $x_j$ \cite{Neumann1945}. 
It is assumed that there are $n$ goods and $m$ processes with $n<m$, which are characterized by constant ratios of inputs to outputs. The model is formulated in terms of an output matrix $B$ and an input matrix $A$. Since the total amount of good produces must match the internal and external demand, described by the positive vector $\mathbf{d}$, we have the equation 
\begin{equation}
\mathbf{B} \cdot \mathbf{x} = \mathbf{A} \cdot \mathbf{x} + \mathbf{d}.
\end{equation}

This economic model can be directly mapped onto a chemical reaction network, if goods are interpreted as chemical species and the vector of intensity of the various processes $\mathbf{x}$ as chemical fluxes. To formalize this analogy, it is convenient to split the stoichiometric matrix $\mathbf{S}$ into the part that concerns the production of products denoted $\mathbf{S}^+$ and the part that concerns the consumption of goods or species $\mathbf{S}^-$, so that we can use $\mathbf{B}=\mathbf{S}^+, \mathbf{A}=\mathbf{S}^-$ and $\mathbf{S}=\mathbf{S}^+ - \mathbf{S}^-$. We say that an economy is productive when
there exists a non-negative vector $\mathbf{x}$ such that 
$\mathbf{B} \cdot \mathbf{x} \ge \mathbf{A} \cdot \mathbf{x}$. This condition maps exactly to the notion of productivity introduced in Eq. \ref{AUT:eq:def_autocat} for stoichiometric autocatalysis.

Obviously, we must require the positivity of the vectors $\mathbf{x}$ and the condition $\sum_i x_i >0$. Another condition is that 
$S_{ij}^++S_{ij}^->0$, which ensures that the economic system is irreducible. This means that the system can not be decomposed into isolated independent subparts.
To show the existence and unicity of the dynamic equilibrium, Von Neumann introduced the function 
\begin{equation}
\label{AUT:eq:VN_1}
    \alpha(\mathbf{x})= \min_i \frac{\sum_j S_{ij}^+ x_j}{\sum_j S_{ij}^- x_j}, 
\end{equation}
Von Neumann proved that the functions $\alpha$ and $\beta$ are uniquely defined from the vector $\mathbf{x}$, which is itself part of the solution. Together they define what is called a dynamic economic equilibrium. In this case, one can show that $\alpha=\max_{\mathbf{x}} \alpha(\mathbf{x})$  represents a maximum expansion coefficient of the economic system. Note that this maximum growth factor is only evaluated based on stoichiometry, it reduces to the dynamical growth factor only if the network is irreducible and mass-action law is used.


\begin{Ecoblock}[title={Production functions}, float=t!,label={AUT:box:production_functions}] 

In economics, production functions relate the quantities of outputs to that of inputs in a system. There are various production functions commonly used in economics, one of the most well-known is the Cobb-Douglas production function \cite{CobbDouglas1928, GrilichesMairesse1999}, which can be written in a general way:

\begin{equation}
    y = c \prod_i x_i^{\alpha_i},
\end{equation}

where $x_i$ are production factors (which could be labor, capital or other factors), $y$ is an amount of product and $c, \alpha_i$ are positive coefficients. Mathematically, this law bears similarities with the mass action law in chemistry, where the rate of production of a species is related to the product of the concentrations of the reactants to the power of their associated stoichiometric coefficient. A more general production function is the constant elasticity of substitution (CES) production function \cite{KlumpMcAdam2012}. This function accounts for the fact that one product may be substituted with another one and can be written in the form:
\begin{equation}
    y = c \left(\sum_i \alpha_i x_i^{\rho}\right)^{\frac{1}{\rho}}.
\end{equation}

Here, $\rho$ is the coefficient of substitution and $\alpha_i$ is the weight of the production factor $i$ in the total production ($\sum_i \alpha_i = 1$). Instead of $\rho$, one often uses the elasticity of substitution $\sigma$ which is such that $\rho = \frac{\sigma - 1}{\sigma}$.

Here it is assumed that the elasticity of substitution is the same for all pairs of production factors. Note that $\sigma$ could also be defined as an elasticity coefficient that compares the change in the ratio of inputs to changes in the ratio of marginal products. Indeed, the marginal product with respect to factor $j$ is $y_j = \partial y / \partial x_j = c \rho \alpha_j x_j^{\rho - 1}\left(\sum_i \alpha_i x_i^{\rho}\right)^{\frac{1-\rho}{\rho}}$ and represents the sensitivity of the product to the amount of production factor $j$. Therefore, $y_j/y_k = \alpha_j x_j^{\rho-1}/\alpha_k x_k^{\rho-1}$, and we recover that :

\begin{align}
    \sigma = \frac{\partial \ln(x_j/x_k)}{\partial \ln(y_k/y_j)}.
\end{align}

Therefore, $\sigma$ measures precisely how the ratio of two production factors is modified when the ratio of two corresponding marginal products is modified. Now, three famous cases can be considered \cite{KlumpMcAdam2012}:
\begin{itemize}
    \item $\sigma \to \infty$ : In this case, any modification in the ratio of marginal products would require an infinite modification in the ratio of production factors. This means that the ratio of marginal products remains constant whatever the modification in the ratio of production factor. This is the so called \emph{perfect factor substitution} limit. Indeed, the CES production function becomes linear ($\rho = 1$), $y = c\sum_i \alpha_i x_i$, and any production factor can be substituted by another (even if $\exists i, \, x_i = 0$, it can be replaced by any other $x_j$).
    \item $\sigma = 1$ : In this case, a modification in the ratio of marginal products translates to the same modification in the ratio of production factors. If we want the dependency of the production in production factor $j$, we need to double the amount of production factor $j$. This limit corresponds to $\rho\to 0$, in which case we recover the \emph{Cobb-Douglas} production function $y=c \prod_i x_i^{\alpha_i}$. This means that the substitutability of a any production factor is null (if one of the $x_i = 0$, then $y=0$). Interestingly, for all values of $\sigma \in [0,1]$, there is no possible substitution between production factors.
    \item $\sigma = 0$ : This case is particularly interesting, it means that the ratios of the production factors $x_j/x_k$ are fixed whatever the ratios $y_j/y_k$. In this limit we recover the so called \emph{Leontief function with fixed factor proportions}. There is still no substitution between the products (if one $x_i = 0$, then $y=0$), but this also means that forming one unit of product always requires the production factors in the same proportions. This corresponds to $\rho \to -\infty$, which gives $y = c\min(x_i)$. Interestingly, this is a way to get rid of the coefficients $\alpha_i$ which represent the share of each production factor in the total production. This boils down to saying that production is limited by the scarcest resource.
\end{itemize}

\end{Ecoblock}

\subsection{Leontief's production function}
\label{AUT:subsection:Leontief}


Wassily W. Leontief won the Nobel prize in economics for his work on input output relations in economic systems, which he started in 1936 \cite{Leontief1936}. He later developed this mathematical tool to study the American economy, and in particular the interdependency between industries. 

The Leontief model is the special case of the von Neumann model, in which each productive activity has a single output (no joint products) whereas there may be many activities producing the same output and each good is produced by at least one industry. In the notation of the previous section, this means that $\mathbf{S}^+$ is the identity matrix. 
%It is possible to build the matrix $a_{i,j}$ of fluxes between industries, that is the number of goods from an industry $i$ required to build one unit of a good from an industry $j$ \cite{MustafinKantarbayeva2018}. 
As we saw previously for the Von Neumann model, it is also assumed in the Leontief model that goods are produced with fixed ratios of production factors. This assumption leads to the Leontief production function discussed in the box \ref{AUT:box:production_functions}.

We can think of this production function as the outcome of a supply chain where production factors have to be assembled in fixed proportions in order to form a product. For instance, in order to build one bike, you need two wheels, one saddle, two pedals, etc... Those ratios are fixed, or equivalently the elasticity of substitution is $\sigma = 0$ as discussed in the box \ref{AUT:box:production_functions}. If you want to form a product $P$, for which you need to assemble $n_1$ units of $R_1$, $n_2$ units of $R_2$, ... up to $n_N$ units of $R_N$, the production rate will be limited by the smaller value of $R_j/n_j$, that is the number of sets of resource $j$ required to produce $P$. If the minimum time to produce one unit of $P$ is $\tau_P$, and the minimum time to use resource $R_j$ in order to produce one $P$ is $\tau_i$, we can write

\begin{equation}
    \frac{dP}{dt} = \frac{1}{\tau_P}\min \left(\frac{\tau_P}{\tau_1}\frac{R_1}{n_1}, \frac{\tau_P}{\tau_2}\frac{R_2}{n_2}, ..., \frac{\tau_P}{\tau_N}\frac{R_N}{n_N} \right).
\end{equation}

Indeed, $\tau_P/\tau_j n_j$ is the number of products which can be simultaneously produced from one resource $j$. Now this result holds true if resources are fully allocated to the production of $P$. Now if you want to produce several products $P_i$ in parallel, one resource may be used by different production chains simultaneously, meaning that a fraction $\alpha_{i,j}$ of total available resources $R_j$ must be used for the specific product $P_i$, so that :

\begin{equation}
    \frac{dP_i}{dt} = \frac{1}{\tau_P}\min \left( \alpha_{i,1}\frac{\tau_P}{\tau_1}\frac{R_1}{n_1}, \alpha_{i,2}\frac{\tau_P}{\tau_2}\frac{R_2}{n_2}, ..., \alpha_{i,N}\frac{\tau_P}{\tau_N}\frac{R_N}{n_N} \right).
\end{equation}

Then the prefactor before $R_i$ represents the maximal number of copies of the product that you can produce simultaneously from one unit of resource $i$. The fact that resources may not be substituted with other resources has important consequences for cell  metabolism \cite{RoyGoberman2021}.

In the problem \ref{ex:Leontief}, we study a single resource - single product industry or workstation and we show that a Leontief production function emerges from mass-action law kinetics when a certain time scale separation holds. This example is important because it illustrates that the Leontief production function not only involves fluxes associated to reactions or industries but can typically also include stocks associated to goods or metabolites, which are only available in finite amounts. 

\subsection{Liebig's law}
\label{AUT:section:liebig}

Interestingly, the idea that the production rate could be limited by the scarcest resource is present in the field of agronomy under the name of  \textit{Liebig's law of minimum} \cite{BloomChapin1985,VanDerPloegBohm1999,TangRiley2021,Echavarria2023}. It was initially used to describe plant growth, which requires various resources, and where it is observed that varying the amount of fully available resources did not modify the final production. This suggests that only scarce resources will limit production and translates to the principle that when a population is growing using various resources, the scarcest will set the growth rate, and the others will be consumed accordingly. This law can be used to model growth of an organism in an environment where resources are constrained. The link between mass action laws and Liebig's law of minimum has been studied \cite{TangRiley2021}. The interest of this method is to obtain equations that are easier to solve on domains where one resource is scarcest. One main difference between the Leontief production function and Liebig's law of the minimum is that for the latter, the minimum is not necessary taken between the numbers of each production factor, but between the yields of those production factors (which can be non-linear functions) \cite{MustafinKantarbayeva2018}. In particular, this means that the rate of production is set by the minimum of the yields of each production factor :

\begin{equation}
    \frac{dY}{dt} = \frac{1}{\tau}
    \min\left(\{f_i(x_i)\}_i\right),
\end{equation}

\noindent where $f_i$ are functions of the production factors $x_i$. To model the requirements of plants in nutrients, yields given by Michaelis-Menten kinetics can be used \cite{TangRiley2021}. Instead of directly comparing the numbers of each production factors, it consists in comparing the yields. However, the idea that one resource will be limiting remains the same.

\subsection{Application to metabolism}
\label{AUT:section:metabolism}

The law of minimum was used to build simplified models of metabolism as an ensemble of coupled autocatalytic cycles \cite{RoyGoberman2021}. Metabolism is seen as a supply chain, where production factors must first be produced and then assembled in fixed proportions to form a product. We call $P$ the number of proteins of one type, produced by translating mRNA (of number $m$) with ribosomes (of number $R$) and substrates (of number $S$). Using the Leontief production function, we can write (following the method of \cite{RoyGoberman2021}) :

\begin{equation}
    \left.\frac{d P}{d t}\right|_{\text{prod}} = \frac{1}{\tau_P} \min\left(\alpha_P R, \alpha_P S, \frac{\tau_P}{\tau_e s_R}m, ...\right)
\end{equation}

Ribosomes and substrates have to be used simultaneously to produce different types of proteins, $\alpha_P$ is the fraction of the total population of ribosomes (and substrates) used to produce the particular protein $P$. The minimum time to produce one $P$ is $\tau_P$, the minimum time to elongate the polymer by one amino acid is $\tau_e$, and $s_R$ is the size of the domain on the mRNA that has to be dedicated to the production of one polymer $P$ at a given time. As explained in \cite{RoyGoberman2021}, $\tau_P/\tau_e s_R$ is then the maximum number of ribosomes that can translate simultaneously one mRNA, and thus the maximum number of copies of $P$ you can produce simultaneously from one unit of mRNA. This is indeed what was predicted from the Leontief's model for input-output systems : the prefactor before every amount of resources is the maximum number of copies of the protein you can produce simultaneously from this specific resource. This term is that of production of the protein, now the protein could be consumed to form a product, or degraded.

To use ribosomes, you first need to form ribosomes by assembling proteins and ribosomal RNA. Similarly, to use mRNA you must first polymerize RNA... This suggests that 
a minimal autocatalytic network that begins to capture the structure of the
transcription-translation machinery is made of two coupled autocatalytic cycles associated respectively to RNA and ribosomes. These two cycles will then be described by coupled equations with production terms described by Leontief's production function \cite{RoyGoberman2021}.

To use Leontief's production function in order to describe metabolism, metabolic autocatalytic cycles are viewed as supply chains. In living cells, ribosomes are produced by assembling ribosomal RNA and proteins, and vice versa proteins require ribosomes and mRNA to be formed. For ribosomes, ribosomal RNA and proteins are building blocks that have to be available in given proportions to produce one additional ribosome.


\section{Concluding remarks}

The expanding economic model of von Neumann (1945) and the input-output model of Leontief
(1936, 1941) laid the foundation of a modern framework for economic analysis. Their framework turned out to be essential to quantify the relative interdependency of various parts of an economy and the nature and the structure of economic equilibria. Remarkably, these tools continue to inspire developments in other fields, such as recent studies of autocatalytic chemical networks.

In this chapter, we showed that the idea of a productive economy, already present in the von Neumann-Leontief framework, is related to the notion of stoichiometric autocatalysis. Similar algorithm based on linear programming methods are now used to identify autocatalytic subnetworks. We also presented some applications of the Von Neumann-Leontief framework for cell metabolism.
%

\section*{Recommended readings}
This book chapter is mainly based on these two recent articles : Anjan Roy et al., A unifying autocatalytic network-based framework
for bacterial growth laws, Proc. Natl. Acad. Sci. U.S.A. 118(33):e2107829118 (2021) and A. Blokhuis et al., Universal motifs and the diversity of autocatalytic systems, Proc. Natl. Acad. Sci. U.S.A., 117, 25230 (2020). We thank R. Pugatch for comments.



\begin{exercise}[Production function of a workstation with a single resource and single product]
\label{ex:Leontief}
We model a workstation with a single resource and a single product \cite{MustafinKantarbayeva2018}. Let $x$ be the amount of available resource or stock, $r$ the supply rate of this stock, $q$ the specific rate with which the stock is being lost or degraded. Further, let $u$ (resp. $v$) be the number of idle (resp. busy) machines which can process the stock. The processing time is $1/\beta$, $b$ is the number of output products per machine, $a$ is rate of capture of stock by machines, $\alpha$ is how many machines get involved per unit resource/stock.

\begin{enumerate}
\item Assuming mass action law for the processing of the stock, show that the equations of the problem are 
\begin{eqnarray}
    \dot{x} &=& r -a u x -q x, \\
    \dot{u} &=& \beta v  -\alpha  u x , \\
    \dot{v} &=& -\beta v  +\alpha  u x , \\
    \dot{y} &=& b  v. 
\end{eqnarray}
    
\item Show that the total number of machines is a constant denoted $u_0$. Derive the steady state number of busy machines $\bar{v}$ in terms of the steady state amount of stock $\bar{x}$. Comment on the form of the output production that you find.

\item Introduce the variables $\gamma=q/(a u_0)$ and $\rho=\alpha r /(a \beta u_0)$. Derive the expression of 
$\bar{v}$ in the limit $\gamma \ll 1$ and $\gamma \ll | \rho -1 |$ and show that it has a Leontief form.
Interpret this form in terms of how the stock is handled in the regime $\rho <1$ and $\rho>1$.

\item Show that for small but non-zero $\gamma$, the input-output function falls below the line boundary defined by the Leontief function.

\end{enumerate}
\end{exercise}

\begin{exercise}[UPF model]
\label{ex:UPF}

The UPF model is a toy model of metabolism made of two coupled autocatalytic cycles \cite{RoyGoberman2021}. In the model, a fraction $\alpha$ of machines of type $U$ catalyze themselves. The remaining machines synthetize another type of machines $P$. The $P$ machines convert an external substrate $f$ to an internal substrate $F$, which is used by $U$ to make more copies of itself and new $P$s. To simplify, we assume that to make one more unit of $U$ by the first reaction, one unit of $F$ is needed, and one unit to make one unit of $P$ by the second reaction.

The model is defined by the following set of equations
\begin{gather}
     \ce{U  + F  ->[$\alpha$] 2U}, \hspace{1em}  \\
    \ce{F + U  ->[$1-\alpha$] P + U}, \hspace{1em}  \\
    \ce{f + P  -> F + P},
    \hspace{1em} \\
    \ce{U -> \emptyset}.
\end{gather}

\begin{enumerate}
    \item Let $n_i$ be the number of molecules of type $i$, where $i \in \{ U,P,F,f \}$. The life type of a machine of type $U$ is $\tau_L$, the incorporation time of $f$ into $F$ is $\tau_F$ and the incorporation time of one unit $F$ is $\tau_a$. Show that within the framework of Leontief production functions, the equations of the model are
 \begin{eqnarray}
        \frac{d n_U}{d t} &=& \frac{\alpha \min(n_U,n_F)}{\tau_a}- \frac{n_U}{\tau_L}, \\
        \frac{d n_P}{d t} &=& \frac{(1-\alpha) \min(n_U,n_F)}{\tau_a}, \\
        \frac{d n_F}{d t} &=& \frac{\min(n_P,n_f)}{ \tau_F}- \frac{\min(n_U,n_F)}{ \tau_a},
    \end{eqnarray}

    \item Discuss the four limitation regimes of the model: (i) $n_F \gg n_U$ and $n_f \gg n_P$, (ii) $n_F \gg n_U$ and $n_f \ll n_P$, (iii) $n_F \ll n_U$ and $n_f \gg n_P$ and (iv) $n_F \ll n_U$ and $n_f \ll n_P$. Simplify the equations for each regime by introducing a common growth rate $\mu$.

    \item Summarize your results by deriving the growth laws of the various regimes in a single plot representing $\alpha$ vs. $\mu$.
\end{enumerate}

\end{exercise}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       please do not edit this block                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifx\weAreInMain\undefined                                                      %
\bibliographystyle{unsrtnat}                                                    %
\bibliography{\chapterroot bibliography-chapter}                                %
\end{document}                                                                  %
\fi                                                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{First section title} 
Add labels to the sections of your chapter and the visual items (equations, tables, figures). Use the convention of \texttt{XXX:type:description}, where \texttt{XXX} is the chapter code, \texttt{type} represents the item type, which is followed by a \textbf{meaningful} description (maybe related to the title of your figure). Do not use \texttt{figure1} or \texttt{equation3} as descriptors!
\label{TEMPLATE:sec:introduction}

Every chapter should start either with a biology question or with an economic analogy, which then leads to the specific topic of the chapter (e.g.\ a modeling formalism). This should be all done in the introductory section here. However, you should call it differently than just "Introduction". Also, please don't capitalize section titles - it should read "Chapter title", not "Chapter Title".

In your chapter, please follow the outline in your chapter draft (see the index in this \href{https://docs.google.com/document/d/1oOBfnEJrt-PZJAblwgY_zM5TBRiAifP9jW6w9gZap9w/edit\#}{overview Google doc}).
If you change the chapter structure during writing, please also update the chapter draft (to inform other authors). Note that you can structure your chapter into sections and subsections, but using lower levels of \LaTeX{} structuring is not permitted. Orient your chapter length towards 10-15 pages, and in the final version, each chapter should contain material for one class and exercises that go with that class.

%Please provide a list of recommended readings (just write the full references as a list, not using bibtex). Possibly, add little comments on why (or for what) this reading is useful like in the following example.

%\textbf{A nice book} A good book that will tell you about evolution. Charles Darwin. The Origin of Species. Cambridge University Press, 6 edition, 2009. doi: 10.1017/93 cbo9780511694295.

We have adopted several macros to leave notes and facilitate collaborative writing, as \texttt{\textbackslash usepackage\{todonotes\}} was included into the \texttt{macros-chapter.tex}. You can leave your comments as to-dos\todo{I am a to-do.}. Alternatively, you can also use the latex command \texttt{\textbackslash{comment}} to \comment{comment the text, which will be highlighted in red}. The to-do's and comments will not be present in the compiled versions of the book - mind your punctuation to avoid double spaces and missing punctuation marks.

\subsection{In-text references}
References (to subsections, figures, etc) within a chapter work like this: see Section~\ref{TEMPLATE:sec:introduction}. We also have a special command for cross-chapter referencing: see Chapter \texttt{\textbackslash  chapterref\{CEN\}}. In all cases, please use uppercase for your references, e.g. "Chapter 3", or "Table 1.2".

\subsection{Index items}

Please add index items to your text. Use the command \texttt{\textbackslash index\{word\}}, where “word” should be written in lowercase, unless it’s a proper name. The command does NOT replace the word in your text, but needs to be added, e.g. “FBA \texttt{\textbackslash index\{flux balance analysis (FBA)\}”}.


%\section*{Problems}
%Use the \texttt{exercise} environment for your problems %and refer to them in the main text using the \texttt{ref} command as with other display items. Put the solutions inside a separate file 'solutions.tex' (if you provide solutions, please contact the book team). If your problems make use of code or files on the internet, please contact the book team about it.

\section{Analytical solutions for optimal metabolic states}
\label{OEP:Appendix:sec:analyticalSolutions}

The enzyme-control rule (\ref{OEP:eqn:enzymeControlRule}) is a condition for optimal metabolic states, independent of the type of rate laws considered. But what are the actual shapes of optimal enzyme profiles? How do enzyme levels vary across the network, and what are the (kinetic, thermodynamic, or cost) factors that affect them? The enzyme-control rule does not contain any kinetic details and thus cannot provide a complete answer. On the other hand, the numerical methods discussed in previous chapters are not suitable for deriving general principles. Hence, it would be good to have analytical solutions, which exist for unbranched pathways with some simple rate laws. Here, we present only the solutions themselves, but you can find the derivations in \citet{NoorLiebermeister2024}. An overview is given in Table \ref{OEP:tab:AnalyticalFormulae1}.

\subsection{Michaelis-Menten rate law}
\label{OEP:Appendix:sec:MichaelisMenten}

The Michaelis-Menten rate law 
\begin{equation}\label{OEP:Appendix:eqn:ana_mm}
	\fluxi = \enzconci \cdot \kcati~\frac{\smet{i-1}}{\smet{i-1} + \kmi}
\end{equation}
describes irreversible reactions with simple saturation kinetics. In an unbranched pathway at steady state, all rates must be equal. To describe them, we introduce a new variable $\fluxJ$ called the pathway flux, and require that $\forall i~\fluxi = \fluxJ$. Now we can use Eq.~(\ref{OEP:Appendix:eqn:ana_mm}) to find a relationship between substrate and enzyme levels:
\begin{equation}
	\enzconci = \frac{\fluxJ}{\kcati} \cdot \left( 1 + \frac{\kmi}{\smet{i-1}} \right)
 =  \fluxJ \left( \parthr_i + \frac{\parmm_i}{\smet{i-1}} \right)
\end{equation}
where we defined $\parthr_i \equiv 1/\kcati$ and $\parmm_i \equiv \kmi/\kcati$ for convenience.
Combining this equation with an upper bound on total enzyme from Eq.~(\ref{OEP:eqn:enzyme_total}) we get that:
\begin{equation}
	\etotPW \geq \sum_i \enzconci = \sum_i \fluxJ ~\left( \parthr_i + \frac{\parmm_i}{\smet{i-1}} \right) = \fluxJ  \left( \sum_i \parthr_i + \sum_i\frac{\parmm_i}{\smet{i-1}} \right)
\end{equation}
and by rearranging we obtain:
\begin{equation}
	\fluxJ \leq \frac{\etotPW}{\sum_i \parthr_i + \sum_i\frac{\parmm_i}{\smet{i-1}}} \label{OEP:Appendix:eqn:michaelis_menten_bound}.
\end{equation}

Maximizing $\fluxJ$ would mean that we actually reach the upper enzyme bound, and therefore we can also treat this as an equality. Since $\etotPW$ is constant and since the metabolite concentrations are the only free variables, the maximal flux is reached when the denominator on the right-hand side is minimized. But here we encounter a problem: the denominator is a monotonically decreasing function in $\smet{i}$ (for each $i$) and since metabolite concentrations are unbounded, the optimum would be at $\smet{i} \rightarrow \infty$ which, formally, is not a mathematically defined state. And even if we consider this state as a limiting case, the state would not be controllable, but instead very fragile against any changes of enzyme levels.  In reality, of course, this will not happen: the range of physiological osmotic pressures does impose some constraint on the concentrations of small molecules. As a proxy for this effect, we can add another constraint to bound the sum of all metabolite concentrations, $\sum_i \smet{i} \leq \Stot$. Now, one can show  that the optimal allocation of enzymes will obey:
\begin{equation}\label{OEP:Appendix:eqn:allocationForMM}
    \opt{\enzconci} \propto \parthr_i + \sqrt{\parmm_i} \cdot \frac{\sqrt{\normhalf{\parmm}}}{\Stot}
\end{equation}
where $\boldsymbol{\parmm}$ is the vector of all $\parmm_i$ and $\normhalf{\mathbf{x}}=(\sum_i \sqrt{x_i})^2$ is the l$_{1/2}$ norm (i.e., $\normhalf{\parmm} \equiv \left(\sum_i{\sqrt{\parmm_i}}\right)^2$). In this case, the maximal flux would be:
\begin{equation}
    \opt{\fluxJ} = \etotPW \cdot \left(\normone{\parthr} + \normhalf{\parmm} / \Stot\right)^{-1}\,.
\end{equation}

Note that the solution looks essentially the same even if we constrain the first metabolite ($\smet{0}$) to have a fixed concentration.  We will revisit this case in section \ref{OEP:sec:CellModel} in the context of a course-grained model of a growing cell.

If the metabolite density constraint is not very tight (i.e.~$\Stot$ is large enough), we can ignore the second term in Eq.~(\ref{OEP:Appendix:eqn:allocationForMM}), which would be equivalent to assuming all enzymes are substrate-saturated. In this case, the optimal allocation of enzymes will be proportional to $\parthr_i$ (or inversely proportional to $\kcati$) and therefore:
\begin{equation}\label{OEP:Appendix:eqn:PSA}
\lim_{\Stot \rightarrow \infty}	\opt{\fluxJ} = \etotPW \cdot \left(\normone{\parthr}\right)^{-1}\,.
\end{equation}
Interestingly, $\normone{\parthr}$, which is equal to the sum of $\kcat$ reciprocals, is in fact the inverse of the \textit{Pathway Specific Activity} (PSA) as originally defined by \citet{BarEvenNoor2010}. Indeed, the idealized scenario considered in that study (where all enzymes were irreversible and saturated) provides an upper bound on the maximal flux achievable. Interestingly, if we see the $\alpha_i = 1/\kcati$ as minimal enzymatic turnover times and $\alpha_{\rm PW} = \etot/\fluxJ$ is the turnover time of the pathway, we can simply rephrase Eq.~(\ref{OEP:Appendix:eqn:PSA}) by saying that, like in the case of the PSA, turnover times along a pathway are additive. In fact, this holds more generally for any rate law $v_i=e_i ~k_i(\mathbf{s})$, if the enzyme turnover times are defined as $\tau_i=1/k_i$, and resembles the fact that electric resistances in a series of resistors are additive.

One of the reasons the irreversible Michaelis-Menten model is unrealistic is that it does not capture reactions that are close to equilibrium and therefore suffer from a counter-productive reverse flux. This is yet another reason why it might be impossible for some metabolites to reach very high concentrations: they might be products of unfavorable reactions. In most metabolic networks, about half of the reactions are reversible and therefore it would be more realistic to use a reversible rate law such as Haldane's. However, using the Haldane equation would typically create a system of equations for which no analytical solution is known. So, first, we will consider rate laws that account for the reverse flux but, for simplicity, ignore substrate saturation.

\subsection{Thermodynamic rate law}
\label{OEP:Appendix:sec:AnalyticalSolutions_thermodynamic}

Irreversible rate laws like the Michaelis-Menten kinetics depend on substrate concentrations only; in the formula there is no product-dependent reverse term that would decrease the total rate or could make it become negative. However, according to thermodynamics such laws can only be  approximations: thermodynamically feasible rate laws must contain a reverse term and must depend on the thermodynamic imbalance of substrate and product concentrations expressed by the thermodynamic driving force.  Some rate laws can even be written as functions of the thermodynamic force alone. Here is an example of such a ``thermodynamic'' rate law, where $\flux$ is proportional to the thermodynamic force efficiency $1-\mbox{e}^{-\drivingforce}$, while the saturation efficiency $\etakin$ is assumed to be constant:
\begin{equation}
  \label{OEP:Appendix:eqn:ana_thermo}
	\fluxi = \enzconci ~ \kcati \left(1 - e^{-\drivingforce_i}\right) = \enzconci~\kcati \left(1 - \frac{\smet{i}}{\smet{i-1}} ~\frac{1}{\Keqi}\right)\,.
\end{equation}
This type of kinetics approximates cases where all reactions are saturated ($\smet{i} \gg \kmi$), and therefore the thermodynamic force efficiency $\etakin$ in Eq.~(\ref{OEP:eqn:factorized_rate_law}) becomes 1. The parameters here are the turnover numbers $\kcati$ and the equilibrium constants $\Keqi$. However, as we will learn soon, the individual $\Keqi$ do not change the result, and only the overall equilibrium constant ($\Keqtot = \prod_i \Keqi$) matters.

As before, we can define the steady-state pathway flux $\fluxJ$ and apply an upper bound on the total enzyme to get:
\begin{align}\label{OEP:Appendix:eqn:thr_flux_upper_bound}
    \begin{split}
    	\etotPW &\ge \sum_i \enzconci = \sum_i \frac{\fluxJ}{\kcati \left(1 - \expe^{-\drivingforce_i}\right)} = \fluxJ \cdot \left( \sum_i \frac{1}{\kcati} \cdot \frac{1}{1 - \expe^{-\drivingforce_i}} \right)
     \\
    \fluxJ &\le \etotPW \cdot \left( \sum_i \frac{1}{\kcati} \cdot \frac{1}{1 - \expe^{-\drivingforce_i}} \right)^{-1} \,.
    \end{split}
\end{align}

Unfortunately, there is no closed-form analytical solution for the maximal rate of this thermodynamic rate law. But we do have something very close which requires rather simple computations. First, we need to invert the functional relationship between the overall driving force ($\drivingforcetot$) and an auxiliary variable $\Psi$ which is defined by the inverse function of:
\begin{equation}\label{OEP:Appendix:eqn:AnalyticalSolutions_thermodynamic_driving_force}
     \ln \left(\frac{\smet{0}}{\smet{n}} \Keqtot\right) = \drivingforcetot = 2 \sum_i \ln\left(\sqrt{\Psi/\kcati} + \sqrt{1 + \Psi/\kcati} \right)
     \,.
\end{equation}

The expression on the right is analytical and strictly increasing in the range $\Psi \in [0, \infty)$, so there is a unique solution that can be found by simple numerical methods. Then, we can use that value to directly calculate the optimal driving forces, enzyme levels and pathway flux:
\begin{align}\label{OEP:Appendix:eqn:AnalyticalSolutions_thermodynamic_flux}
    \begin{split}
    \opt{\drivingforce_i} &= 2\ln\left(\frac{1}{\kcati}\left(1 + \sqrt{1 + \kcati/\Psi} \right) \right) + \ln\left( \Psi \kcati \right)
    \\
    \opt{\enzconc}_i
    &=
    \frac{\opt{\fluxJ}}{2} \cdot \frac{1}{\kcati}\left(1 + \sqrt{1 + \kcati/\Psi} \right)
    \\
    \opt{\fluxJ} &= 2~\etotPW \cdot \left( \sum_i \frac{1}{\kcati} \left(1 + \sqrt{1 + \kcati/\Psi} \right) \right)^{-1}\,.
    \end{split}
\end{align}
An example of what the relationship between the driving force and the optimal flux looks like is illustrated in Figure \ref{OEP:Appendix:fig:ThermodynamicRateLawSolution} for a pathway with two enzymes. Furthermore, a comparison between our exact formula presented here and a numerical solution based on convex optimization \cite{NoorFlamholz2016} shows no difference at all.

These formulae cannot be directly evaluated because of the unknown parameter $\Psi$. To obtain solutions that do not depend on this parameter, we now consider two limiting cases in which $\Psi$ is either infinitely high (very high driving force) or infinitely low (very low driving force).

When the driving forces are very high (i.e. $\drivingforcetot \rightarrow \infty$), the solution for $\Psi$ in Eq.~(\ref{OEP:Appendix:eqn:AnalyticalSolutions_thermodynamic_driving_force}) will approach infinity and  the optimal flux becomes
\begin{equation}\label{OEP:Appendix:eqn:AnalyticalSolutions_thermodynamic_large_force}
    \lim_{\drivingforcetot \rightarrow \infty} \opt{\fluxJ}
    =
    \lim_{\Psi \rightarrow \infty} \frac{2~\etotPW}{\sum_i 1/\kcati \cdot \left(1 + \sqrt{1 + \kcati/\Psi} \right)}
    =
    \frac{\etotPW}{\sum_i 1/\kcati}
    =
    \etotPW \cdot \left(\normone{\parthr}\right)^{-1}
\end{equation}
with parameters $\parthr_i = 1/\kcati$ defined as before. This solution indeed makes sense as it is equivalent to the fully saturated limit in the Michaelis-Menten case (see Section \ref{OEP:Appendix:sec:MichaelisMenten}, Eq.~(\ref{OEP:Appendix:eqn:PSA})).

However, when the driving force in the pathway is limited ($\Psi$ has a finite positive value), this driving force will be split between reactions according to their $\kcati$ values: enzymes with a higher $\kcati$ value will have to pay a higher penalty due to their driving force being closer to 0. On the other hand, slow enzymes will obtain more driving force, which will help them by having a smaller fraction of reverse flux. Notably, the distribution does \textit{not} depend on the reaction equilibrium constants (only on the overall $\Keqtot$). Perhaps this is not that surprising if we consider the fact that the concentrations intermediate substrates and products are unconstrained and therefore we have enough degrees of freedom to adjust to any value of $\Keqi$ as long as the total driving force stays the same.

One can also consider the other extreme where all reactions are close to equilibrium, which means that $\drivingforcetot$ is close to 0 (and therefore also each $\drivingforce_i \rightarrow 0$). In this case, we can approximate Eq.~(\ref{OEP:Appendix:eqn:thr_flux_upper_bound}) by:
\begin{align}
    J \leq \etotPW \cdot \left( \sum_i \frac{1}{\kcati} \cdot \frac{1}{1 - \expe^{-\drivingforce_i}} \right)^{-1} \approx \etotPW \cdot \left( \sum_i \frac{1}{\kcati~\drivingforce_i} \right)^{-1}.
\end{align}
Maximizing $\fluxJ$ under the constraint $\sum_i \drivingforce_i = \drivingforcetot$ yields the solution: 
\begin{align}\label{OEP:Appendix:eqn:AnalyticalSolutions_thermodynamic_small_force}
\begin{split}
    \opt{\drivingforce_i}
    &\approx
    \drivingforcetot ~ \frac{\sqrt{1/\kcati}}{\sum_i \sqrt{1/\kcati}}
    \\
    \opt{\fluxJ}
    &\approx
    \frac{\drivingforcetot ~ \etotPW}{\left(\sum_i \sqrt{1/\kcat}\right)^{2}}
    =
    \etotPW \cdot \frac{\drivingforcetot}{\normhalf{\parthr}}\,.
\end{split}
\end{align}

\begin{figure}[t!]
    \begin{center}
        \input{\chapterroot /images/OEP_OptimalFluxThermodynamicRatelaw}
    \end{center}  
    \caption{\comment{Make this figure a bit smaller? (but keep the absolute font sizes)} The relationship between the maximal flux per enzyme and the overall driving force for the thermodynamic rate law. -- Even though we do not have a closed-form solution for $\opt{\fluxJ}$ as a function of $\drivingforcetot$, we can still plot $\opt{\fluxJ}(\Psi)$ against $\drivingforcetot(\Psi)$ for varying values of $\Psi$ using Eq.~(\ref{OEP:Appendix:eqn:AnalyticalSolutions_thermodynamic_driving_force}) and Eq.~(\ref{OEP:Appendix:eqn:AnalyticalSolutions_thermodynamic_flux}). Here, we show this relationship for a pathway with 2 steps (orange). The parameters are: $\Keqi = 1$, $\kcatn{1} = 3~ s^{-1}$, and the $\kcat$ of the second enzyme is either (A) $\kcatn{2} = 100~ s^{-1}$ or (B) $\kcatn{2} = 2~ s^{-1}$. The dashed lines represent the approximations for: $\drivingforcetot = 0$ as in Eq.~(\ref{OEP:Appendix:eqn:AnalyticalSolutions_thermodynamic_small_force}), $\drivingforcetot \rightarrow \infty$ as in Eq.~(\ref{OEP:Appendix:eqn:AnalyticalSolutions_thermodynamic_large_force}), and the approximation using Eq.~(\ref{OEP:Appendix:eqn:ApproximateSolutions_thermodynamic_flux}) which is based on effective parameters chosen to match the other two at the limits (cyan).}
    \label{OEP:Appendix:fig:ThermodynamicRateLawSolution}
\end{figure}

Using the two limiting cases ($\drivingforcetot \rightarrow \infty$ and $\drivingforcetot \approx 0$), we can approximate the solution from Eq.~(\ref{OEP:Appendix:eqn:AnalyticalSolutions_thermodynamic_flux}) by a much simpler formula which has the same shape as the thermodynamic rate law for a single reaction:
\begin{align}\label{OEP:Appendix:eqn:ApproximateSolutions_thermodynamic_flux}
    \opt{\fluxJ} &\approx \frac{\etotPW}{\normone{\parthr}} \left( 1 - \exp
        \left(
        -\frac{\normone{\parthr}}{\normhalf{\parthr}} \drivingforcetot \right)
     \right)\,.
\end{align}
Figure \ref{OEP:Appendix:fig:ThermodynamicRateLawSolution} compares between the precise and approximate solution for two example cases. One can appreciate that the two curves are nearly indistinguishable, illustrating the good quality of the approximation.

At first, the exponent in Equation \ref{OEP:Appendix:eqn:ApproximateSolutions_thermodynamic_flux} seems like a complicated function of the $\parthr_i$. However, we can show that it can be no larger than one:
\begin{align}
\begin{split}
    \normhalf{\parthr}
    &= \left(\sum_{i=1}^n \sqrt{\parthr_i}\right)^2
    = \sum_{i,j=1}^n \sqrt{\parthr_i} \sqrt{\parthr_j}
    = \sum_{i=1}^n \parthr_i + 2 \sum_{i\neq j} \sqrt{\parthr_i} \sqrt{\parthr_j}
    \geq \sum_{i=1}^n \parthr_i
    = \normone{\parthr}\,.
\end{split}
\end{align}
Furthermore, it can be developed and expressed as a function of the Coefficient of Variation (i.e. the standard deviation divided by the mean) of $\parthr$:
\begin{align}
\begin{split}
    \frac{\normone{\parthr}}{\normhalf{\parthr}}
    &= \frac{\sum_{i=1}^n \parthr_i}{\left(\sum_{i=1}^n \sqrt{\parthr_i}\right)^2}
    = \frac{n \mathbb{E}[\parthr]}{(n~\mathbb{E}[\parthr^\frac{1}{2}])^2}
    = \frac{1}{n} \cdot \frac{\mathbb{E}[\parthr]}{\mathbb{E}[\parthr^\frac{1}{2}]^2}
    = \frac{1}{n} \cdot \frac{\mathbb{E}[\parthr] - \mathbb{E}[\parthr^\frac{1}{2}]^2 + \mathbb{E}[\parthr^\frac{1}{2}]^2}{\mathbb{E}[\parthr^\frac{1}{2}]^2}
    \\
    &= \frac{1}{n}\left(\frac{\mathbb{E}[\parthr] - \mathbb{E}[\parthr^\frac{1}{2}]^2}{\mathbb{E}[\parthr^\frac{1}{2}]^2} + 1\right)
    = \frac{1}{n}\left(\frac{\text{Var}[\parthr^\frac{1}{2}]}{\mathbb{E}[\parthr^\frac{1}{2}]^2} + 1\right)
    = \frac{\text{CV}[\parthr^\frac{1}{2}]^2 + 1}{n}\,.
\end{split}
\end{align}
This expression makes it clear that the lower bound is $1/n$, which can be reached when all $\kcati = \kcat$ (and therefore the CV would be $0$). In summary:
\begin{align}
    \frac{1}{n} \leq \frac{\normone{\parthr}}{\normhalf{\parthr}} = \frac{\text{CV}[\parthr^\frac{1}{2}]^2 + 1}{n} \leq 1
\end{align}
and since $1 - \exp(-x)$ is a monotonically increasing function (in $x$), we can conclude that:
\begin{align}
    \frac{\etotPW \cdot (1 - \exp(-\drivingforcetot/n))}{\normone{\parthr}} 
    \leq
    \Tilde{\fluxJ}
    \leq
    \frac{\etotPW \cdot (1 - \exp(-\drivingforcetot))}{\normone{\parthr}} 
\end{align}
where $\Tilde{\fluxJ}$ is our approximation for $\opt{\fluxJ}$ in Equation \ref{OEP:Appendix:eqn:ApproximateSolutions_thermodynamic_flux}.

\subsection{Mass-action rate law}
\label{OEP:Appendix:sec:reversibleMassAction}

We now consider the same unbranched pathway, but with mass-action rate laws:
\begin{equation}\label{OEP:Appendix:eqn:analyticalMassAction}
	  \fluxi
    =
    \enzconci ~ \kcati/\kmi ~ (\smet{i-1} - \smet{i}/\Keqi)
    =
    \enzconci ~ \parmm_i^{-1} ~ (\smet{i-1} - \smet{i}/\Keqi)
    \,.
\end{equation}
As before, we define $\parmm_i \equiv \kmi/\kcati$. Note that instead of Eq.~(\ref{OEP:eqn:massActionRateLaw}) we write the mass-action rate law in a form that does not require the turnover rate and $\km$ of the product (and instead uses $\Keqi$), to avoid confusing indexation of forward and backward parameters.
Like with the previous rate laws, we define the pathway flux $\fluxJ$ and apply an upper bound on the total enzyme levels:
\begin{align}\label{OEP:Appendix:eqn:massActionFluxUpperBound}
    \begin{split}
    	\etotPW &\ge \sum_i \enzconci = \sum_i \frac{\fluxJ ~ \parmm_i}{ \smet{i-1} - \smet{i}/\Keqi} = \fluxJ \sum_i \frac{\parmm_i}{ \smet{i-1} - \smet{i}/\Keqi}
     \\
    \fluxJ &\le \etotPW \cdot \left( \sum_i \frac{\parmm_i}{\smet{i-1} - \smet{i}/\Keqi}\right)^{-1} \,.
    \end{split}
\end{align}
Again, we maximize the flux at a constrained total enzyme level. Optimization with  Lagrange multipliers yields expressions for the optimal individual enzyme levels ($\opt{\enzconc_j}$) and the maximal flux ($\opt{\fluxJ}$): 
\begin{align}\label{OEP:Appendix:eqn:solutionMassAction}
    \begin{split}
        \opt{\enzconc}_j &= \etotPW ~ \sqrt{ \frac{\parma_j}{\normhalf{\parma}}}
        \\
        \opt{\fluxJ} &= \etotPW \cdot \frac{\smet{0}~\Keqtot - \smet{n}}{\normhalf{\parma}}
    \end{split}
\end{align}

where $\parma_j$ is defined as
\begin{align}
    \begin{split}
    \parma_i \equiv \parmm_i \prod_{j=i}^{n} \Keqj
    \end{split}
\end{align}

Of course, the exact value of this maximum depends on all the different system parameters. However, it is interesting to consider a na\"{i}ve assumption where all the $\parma_j$ parameters are identical.  In such a case, the flux in the pathway would decrease quadratically with the number of steps \cite{HeinrichSchuster2012}. Of course, we know that in real metabolic pathways the equilibrium constants are typically not close to 1, and therefore this approximation might not have many applications in biology.

\comment{Refer to this in the main text?}
What would happen if a mutation improved the catalytic rate of only one of the enzymes $\enzconci$ by a factor of $a$ (i.e. $\parmm_i$ decreases by a factor of $a$, but the equilibrium constant $\Keqi$ remains the same)? In this case, $\parma_i$ would be divided by $a$, but the optimal enzyme concentration for this reaction $\opt{\enzconci}$ would only decrease by a factor of $\sqrt{a}$. This saving would then be distributed proportionally among all the $n$ enzymes and would contribute to an increase in the pathway flux $\fluxJ$. On the flip side, if for some reason the activity of one enzyme is decreased by a multiplicative factor $b$, it would need to ``pay'' (increase the enzyme's concentration) only by a factor $\sqrt{b}$, and this increase would be ``funded'' by all of the enzymes together in order to keep the same $\etotPW$, thus lowering $\fluxJ$.

\subsection{Haldane rate law -- no analytical solutions for pathway efficiency are known}

So far, we analyzed pathway models in which all the enzymes could be described by the same  kinetic rate law: mass-action, Michaelis-Menten, or thermodynamic. Although each of these rate laws can reliably describe some enzymes in specific conditions, it is unlikely that the same approximation would apply to \textit{all} the reactions in a pathway. A more realistic model would allow all reactions to be reversible and saturable, as described by the general rate law (\ref{OEP:eqn:hoh_cord}). The rate law can be written in factorized form which makes it is easier to separate thermodynamics from saturation effects.

\begin{equation}\label{OEP:Appendix:eqn:SeparableRateLaw}
	\fluxi = \enzconci \cdot \kcati \cdot \left(1 - \expe^{-\drivingforce_i}\right) \cdot \frac{ \smet{i-1}/K_{S,i}}{1 + \smet{i-1}/K_{S,i} + \smet{i}/K_{P,i}}\,.
\end{equation}

Aiming at determining the optimal enzyme levels, we may assume that all fluxes are equal to $\fluxJ$, and put an upper bound on the total enzyme budget:
\begin{align}
\begin{split}
    \etotPW \ge \sum_i \enzconci &= \sum_i \frac{\fluxJ}{\kcati \cdot \left(1 - \expe^{-\drivingforce_i}\right) \cdot \frac{ \smet{i-1}/K_{S,i}}{1 + \smet{i-1}/K_{S,i} + \smet{i}/K_{P,i}}}
    \\
    &=
    \fluxJ \cdot \left( \sum_i\frac{1}{\kcati}
    \left(1 - \expe^{-\drivingforce_i}\right)^{-1}
    \left( 1 + \frac{K_{S,i}}{\smet{i-1}} + \frac{\smet{i} K_{S,i} }{\smet{i-1} K_{P,i}} \right)
    \right),
\end{split}
\end{align}
yielding
\begin{align}
\begin{split}
    \fluxJ &\leq \etotPW \cdot
    \left(
    \sum_i \frac{1}{\kcati}
    \left(1 - \expe^{-\drivingforce_i}\right)^{-1}
    \left( 1 + \frac{K_{S,i}}{\smet{i-1}} + \frac{\smet{i} K_{S,i} }{\smet{i-1} K_{P,i}} \right)
    \right)^{-1}.
 \end{split}
\end{align}
As expected, this formula is a generalization of both Eq.~(\ref{OEP:Appendix:eqn:michaelis_menten_bound}) (Michaelis-Menten) and (\ref{OEP:Appendix:eqn:thr_flux_upper_bound}) (thermodynamic). We can see that the maximal pathway flux would be realized when the term in parentheses is minimized with respect to the $\smet{i}$, i.e.:
\begin{align}
	\text{minimize}_{\vec{s}}\, \sum_i \frac{1}{\kcati} \left(1 - \expe^{-\drivingforce_i(\vec{s})}\right)^{-1}
	\left( 1 + \frac{K_{\rm S,i}}{\smet{i-1}} + \frac{\smet{i} K_{\rm S,i} }{\smet{i-1} K_{\rm P,i}} \right)\,.
\end{align}

Unfortunately, no analytical solution is known for this general problem.


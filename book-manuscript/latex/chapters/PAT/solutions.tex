% Solutions to exercises from Summer school 2022

\paragraph{Problem \ref{PAT:Exerc:HaldaneRateLaw}  (Haldane kinetic rate law)}

First, we add the constraint on the total enzyme concentration ($[E]+[ES]+[EP] = E_\text{tot}$) and rewrite the ODE system in matrix notation:
    \begin{equation}
        \begin{pmatrix}
            1&1&1 \\
            [S] k_1&-(k_2+k_3) & k_4\\
            [P] k_6&k_3 & -(k_4+k_5)\\
            -[S] k_1 - [P] k_6 & k_2 & k_5\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        \begin{pmatrix}
            [E_0]\\
            0\\
            0\\
            0
        \end{pmatrix}.
    \end{equation}
    Note that the last row is linearly dependent on the two previous ones (it is minus their sum). Therefore, we can drop it from the system without loosing information. Then, we will find exlicit expressions for $[E]$, $[ES]$, and $[EP]$ by using Gaussian elimination -- a process of eliminating off-diagonal values in the matrix until we reach the identity matrix, while at the same time applying the same operations to the vector on the right-hand side of the equality.

    Step 1, elimination the off-diagonal elements on the first column (subtracting the first row times $[S] k_1$ from the 2nd row and the first row times $[P] k_6$ from the 3rd row)
    \begin{equation*}
        \begin{pmatrix}
            1 & 1 & 1 \\
            0 & -(k_2+k_3) -[S] k_1 & k_4 -[S] k_1\\
            0 & k_3 -[P] k_6 & -(k_4+k_5) - [P] k_6\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            1\\
            -[S] k_1\\
            - [P] k_6\\
        \end{pmatrix}.
    \end{equation*}

    Step 2, dividing the second row by $-(k_2 + k_3 + [S] k_1)$ to have 1 on the diagonal:
    \begin{equation*}
        \begin{pmatrix}
            1 & 1 & 1 \\
            0 & 1 & \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1}\\
            0 & k_3 -[P] k_6 & -(k_4+k_5) - [P] k_6\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            1\\
            \frac{[S] k_1}{k_2 + k_3 + [S] k_1}\\
            - [P] k_6\\
        \end{pmatrix}.
    \end{equation*}

    Step 3, subtracting the second row from the 1st, and again from the 3rd (after multiplying by $k_3 - [P]k_6$):
    \begin{equation*}
        \begin{pmatrix}
            1 & 0 & 1 - \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1} \\
            0 & 1 & \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1}\\
            0 & 0 & -(k_4+k_5) - [P] k_6 - \frac{([S] k_1 - k_4)(k_3 -[P] k_6)}{k_2 + k_3 + [S] k_1}\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            1 - \frac{[S] k_1}{k_2 + k_3 + [S] k_1}\\
            \frac{[S] k_1}{k_2 + k_3 + [S] k_1}\\
            - [P] k_6 - \frac{[S] k_1 (k_3 -[P] k_6)}{k_2 + k_3 + [S] k_1}
        \end{pmatrix}.
    \end{equation*}
    which after simplifying becomes:
    \begin{equation*}
    \begin{pmatrix}
            1 & 0 & \frac{k_2+k_3+k_4}{k_2 + k_3 + [S] k_1} \\
            0 & 1 & \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1}\\
            0 & 0 & - \frac{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}{k_2 + k_3 + [S] k_1}\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            \frac{k_2+k_3}{k_2 + k_3 + [S] k_1}\\
            \frac{[S] k_1}{k_2 + k_3 + [S] k_1}\\
            -\frac{[P] k_6 k_2 + [P] k_6 k_3 + [S] k_1 k_3 }{k_2 + k_3 + [S] k_1}
        \end{pmatrix}.
    \end{equation*}
    and we normalize the last row to have 1 on the diagonal:
    \begin{equation*}
    \begin{pmatrix}
            1 & 0 & \frac{k_2+k_3+k_4}{k_2 + k_3 + [S] k_1} \\
            0 & 1 & \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1}\\
            0 & 0 & 1\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            \frac{k_2+k_3}{k_2 + k_3 + [S] k_1}\\
            \frac{[S] k_1}{k_2 + k_3 + [S] k_1}\\
            \frac{[P] k_6 k_2 + [P] k_6 k_3 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}
        \end{pmatrix}.
    \end{equation*}

    Step 4, we eliminate the off-diagonal values of the third column using the 3rd row:
    \begin{eqnarray*}
        \begin{pmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0\\
            0 & 0 & 1\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            \frac{k_2+k_3}{k_2 + k_3 + [S] k_1} - \frac{k_2+k_3+k_4}{k_2 + k_3 + [S] k_1} \cdot \frac{[P] k_6 k_2 + [P] k_6 k_3 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}\\
            \frac{[S] k_1}{k_2 + k_3 + [S] k_1} - \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1} \cdot \frac{[P] k_6 k_2 + [P] k_6 k_3 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}\\
            \frac{[P] k_6 k_2 + [P] k_6 k_3 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}
        \end{pmatrix}
    \end{eqnarray*}

    Simplifying the expressions on the right-hand side is a lengthy process (which we do not show here) and in the end we get:
    \begin{eqnarray*}
    \begin{pmatrix}
        1 & 0 & 0 \\
        0 & 1 & 0\\
        0 & 0 & 1\\
    \end{pmatrix}
    \begin{pmatrix}
        [E]\\
        [ES]\\
        [EP]
    \end{pmatrix}
    =
    [E_\text{tot}]
    \begin{pmatrix}
        \frac{k_2 k_4 + k_2 k_5 + k_3 k_5}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}\\
        \frac{[P] k_4 k_6 + [S] k_1 k_4 + [S] k_1 k_5}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}\\
        \frac{[P] k_2 k_6 + [P] k_3 k_6 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}\\
    \end{pmatrix}
\end{eqnarray*}

    Therefore,
    \begin{align}
        [E] &= [E_\text{tot}] \frac{k_2 k_4 + k_2 k_5 + k_3 k_5}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5} \\
        [ES] &= [E_\text{tot}] \frac{[P] k_4 k_6 + [S] k_1 k_4 + [S] k_1 k_5}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5} \\
        [EP] &= [E_\text{tot}] \frac{[P] k_2 k_6 + [P] k_3 k_6 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}
    \end{align}

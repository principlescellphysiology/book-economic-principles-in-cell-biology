dir <- "~/book-economic-principles-in-cell-biology/CEN_plots"
setwd(dir)

set.seed(30)
data <- rpois(2000, 6.7)

svg(filename = "./population_distribution.svg", width=15, height=10, pointsize = 30)
#cairo_pdf(filename = "./population_distribution.pdf", width=15, height=10, pointsize = 30)

par(mar = c(4.5,4.5,0.5,0), lwd = 5)

hist(data, freq = FALSE, breaks = (length(unique(data))-1),
     xlab = "Molecule copies per cell", ylab = "Frequency", 
     cex.lab = 1.5, cex.axis = 1.2, main = NA, 
     xlim = c(0,15), ylim = c(0,0.15),
     border = "white", col = "grey72")

abline(v=mean(data), col = "violetred", lwd = 4)

dev.off()

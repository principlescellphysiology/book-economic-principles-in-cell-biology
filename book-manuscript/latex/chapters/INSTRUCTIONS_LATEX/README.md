Book chapter latex files - example folder
=========================================

PLEASE NOTE - THE FILES IN THIS FOLDER ARE NOT APPROVED YET. WE WILL LET YOU KNOW WHEN A USABLE VERSION WILL BE AVAILABLE

This is a folder with template files for one chapter. To start writing your own chapter, please make a copy of this folder (a zip version of this folder will be available soon) and edit it wherever you like (e.g. on overleaf). Later, copies of your chapter folder will be put into the folder `book-manuscript/latex/chapters/XXX`, where XXX stands for the ID of your chapter (see the parent directory).

In your local copy, you can change the files bibliography.bib, macros.tex, and style.tex. However, these changes will NOT take effect for the compiled version of the book. If you want to make changes there, please request changes via our [Google document](https://docs.google.com/document/d/1K9GBh0DlwDVtga_P0HgNAcdulgRHn_CgTDtZ1l64Dik/edit#heading=h.2rjfgfdcck3x).

For questions about the template files, please contact Markus Köbis.
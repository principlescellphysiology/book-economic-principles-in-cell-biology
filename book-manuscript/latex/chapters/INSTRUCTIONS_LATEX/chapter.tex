%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       please do not edit this block                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifx\weAreInMain\undefined                                                      %
\documentclass[a4paper]{book}                                                   %
\input{./main/macros.tex}                                                   %
\input{./main/style.tex}                                                    %
\input{./macros-chapter.tex}                                                    %
\input{./style-chapter.tex}                                                     %
\usepackage{hyperref}                                                           %
\usepackage[version=4]{mhchem}                                                  %
\begin{document}                                                                %
  \renewcommand{\chapterroot}{./}                                               %
\fi                                                                             %
%                                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is a short guide for the chapter editors and authors on how to design your LaTeX sources
% and how to use the templates
%
\chapter{Latex Instructions and Examples}
\label{INSTRUCTIONS_LATEX}

\hideinbooklayout{
\begin{center}
  {\large\textbf{Version: \today}}%
\end{center}
}

%\includechaptereditors{M.~K\"obis}
\includechapterauthors{M.~K\"obis, W.~Liebermeister, and E.~Noor}

\chapteracknowledgments{M.~K\"obis is the main author\slash editor of this chapter and drafted it with W.~Liebermeister, and E.~Noor.
The authors thank J.~Zanghelini and H.~Sauro for their feedback.%
}

\copyrightinfo{Some of this \LaTeX{} document was taken from the natbib documentation from overleaf under \url{https://de.overleaf.com/learn/latex/Natbib_citation_styles}}

\chapterkeywords{Instructions, Latex macros, ...}

%
\begin{chapterhighlights}
%\section*{Chapter highlights}

\begin{itemize}
	\item In this \LaTeX{} document, authors and editors are introduced to the organizational process of the writing.
	\item They will also be shown the usage of the chapter templates, file organization, and how to submit their work to the main editors.
	\item Some recommendations and help concerning the use of \LaTeX, the bibliography and the graphics are also provided.
	\item Note that none of these rules are set in stone yet.
	The content of your chapter is the main product; style and typesetting can always be dealt with at a later stage.
\end{itemize}
\end{chapterhighlights}

%
%
%

\section{General instructions}
\label{INSTRUCTIONS_LATEX:Sec:General}

This document exemplifies \LaTeX{} conventions and macros used in our book.
For a simpler template file, to be used as a starting point for your chapter, please see the \texttt{TEMPLATE} folder in \href{https://gitlab.com/principlescellphysiology/book-economic-principles-in-cell-biology/-/tree/master/book-manuscript/latex/chapters/}{book-manuscript/latex/chapters/}.
Here are some extra rules.

\begin{itemize}
  \item To propose new macros or conventions, please use this \href{https://docs.google.com/document/d/1K9GBh0DlwDVtga_P0HgNAcdulgRHn_CgTDtZ1l64Dik/edit#}{Google doc}.
  \item Please see
\href{https://docs.google.com/document/d/1K9GBh0DlwDVtga\_P0HgNAcdulgRHn\_CgTDtZ1l64Dik/edit\#}{this
  GoogleDoc} for an up-to-date version of our recommendations concerning submissions to the repository.
  \item Instructions for writing can also be found in this \href{https://docs.google.com/document/d/1DJqJn1iMm9XPx24pGCdF2YA-hPG0_TlreS-AWeKEbuA/edit#heading=h.wmmt05ctneog}{Google doc}
  \item Please note that we plan to publish the book under the \href{https://creativecommons.org/licenses/by-sa/4.0/}{Creative Commons BY-SA} license.
  \item Any technical questions can be sent to \href{mailto:markus.koebis@gmail.com}{Markus}.
\end{itemize}



\section{Infrastructure for writing}
\label{INSTRUCTIONS_LATEX:Sec:Infrastructure}

%
\subsection{Central repository}%
\label{INSTRUCTIONS_LATEX:Subsec:Repo}

The master version of the book project will be hosted
\href{https://gitlab.com/principlescellphysiology/book-economic-principles-in-cell-biology}{here}
on GitLab.
This is also the place where you can find up-to-date \LaTeX{} files shared between chapters (\LaTeX{} macros + bibtex file)
%

%
\begin{itemize}
  \item \textbf{Uploads to central repository:} Chapter editors are encouraged to upload copies of their chapter files to the central repository whenever a major progress was made.
  %
  \item Either send a pull-request or alert (one of) the main editors to make it for you.
  %
  \item If you plan to use Overleaf and prefer having a pro version, ask
  \href{mailto:eladnoor@protonmail.com}{Elad} to create a project for your chapter and share it.
  %
  \item You are free to commit additional material which is not part of the actual chapter (yet) to your designated space in the repository (cf.\ Section~\ref{INSTRUCTIONS_LATEX:Subsec:Folders} below).
  However, please try to keep the repository reasonably organized and do not submit .aux, .bbl, .log files etc.
\end{itemize}


%
%
%
\subsection{Writing process}%

Each chapter team is free to use whatever collaboration tool works best for them.
Final commits will be collected in the git-repository in the form of \LaTeX{} source files.
This does not mean that all chapters have to be drafted using \LaTeX{} but, before they are uploaded to the repository, they will be converted and afterwards changes can only be applied directly to those files.
We encourage the use of \href{https://www.overleaf.com}{overleaf}.


\subsection{Chapter and folder organization}%
\label{INSTRUCTIONS_LATEX:Subsec:Folders}

Each chapter team will be assigned a \emph{three-letter chapter shortcut}.
The shortcuts can be found in the \href{https://docs.google.com/document/d/1oOBfnEJrt-PZJAblwgY\_zM5TBRiAifP9jW6w9gZap9w/edit\#}{table of contents}.
In the final repository, all contributions from a chapter team will then be collected in a sub-folder \texttt{./chapter/<XXX>} where \texttt{<XXX>} denotes the chapter shortcut.
Within this sub-folder, chapter authors can organize their material as they wish, i.e.\ upload files and\slash or create subdirectories.
We recommend that you dedicate a subdirectory \texttt{./images} for graphics.


\paragraph{Chapter Templates}

The structure of a new chapter should follow the points given in \href{https://docs.google.com/document/d/1N_eACJjfkXog4tAUZbwH-ZmoIjHHkQ2k1OO1BBdd9rQ/edit#}{this GoogleDoc.}
%
Once a chapter team's proposal was assigned a chapter shortcut and approved by at least two reviewers from the group (i.e.\ not the chapter team itself), the actual writing can start.
We provide this document and its corresponding \LaTeX{} source (chapter \texttt{INSTRUCTIONS\_LATEX}) as a guide and an additional template chapter \texttt{TEMPLATE}.
The complete template includes
\begin{itemize}
  \item{a file \texttt{chapter.tex}:
  This file is written in such a way that it can be used as a replacement for the main document of the book project when drafting a chapter; a pdf output can then be generated using \texttt{pdflatex {\color{gray}-shell-escape} chapter.tex}.
  We encourage you to use \texttt{macros-chapter.tex} to define \LaTeX-commands and reserve \texttt{chapter.tex} for the actual writing.
  }
  \item{a file \texttt{macros-chapter.tex}:
  This (initially almost empty) file can be used to define \LaTeX{} macros for each chapter.
  Please do not define (global) macros in other \texttt{.tex} files, especially not in \texttt{chapter.tex}.
  It is allowed (but not encouraged) to use \LaTeX's \texttt{\textbackslash renewcommand} here to overwrite global definitions from the main part of the book.
  }
  \item{a file \texttt{style-chapter.tex}:
  This (initially (almost) empty) file can be used to re-define style-related options (fonts, annotations, spacing etc.).
  }
\end{itemize}
Note that any definitions made within the last two files will \textbf{not} (automatically) be transferred to the main book.
The book editors will later on decide on macros that will be transferred to the main document.
Proposals for such macros can be made
\href{https://docs.google.com/document/d/1K9GBh0DlwDVtga_P0HgNAcdulgRHn_CgTDtZ1l64Dik/edit#}{here} {\color{red}using the commenting functionality of GoogleDocs} or by contacting the main editor team.
This includes also wishes for certain packages needed in the finalized version of the book.
%

%
In the final version of the book, the \texttt{chapter.tex} files from all chapter teams will be assembled and jointly compiled into one document using the \texttt{\textbackslash input\{\}} command of \LaTeX.
At that point, none of the files \texttt{macros-chapter.tex} or \texttt{style-chapter.tex} will be used anymore.
We generally recommend the use of \texttt{\textbackslash input\{\}} when working with sub documents within a chapter draft.
Note that references to local files will finally be necessary at this stage.
The templates therefore come with a macro \texttt{\textbackslash chapterroot} which should precede all directory references, see the \texttt{\textbackslash includegraphics\{\}} example below.


\subsection{Sectioning}
\label{INSTRUCTIONS_LATEX:Subsection:sectioning}
%

The highest level sectioning in a chapter is the \texttt{\textbackslash chapter} command.
For your sectioning, consider \verb@\section@, \verb@\subsection@, and \verb@\paragraph@ and please avoid other levels of granularity in the numbering.


\subsection{Custom boxes}

\begin{Ecoblock}[label={INSTRUCTIONS_LATEX:Ecoblock:intro},title={Using boxes}]
The template chapters come equipped with an \texttt{Ecoblock} environment.
If you present economic analogies to motivate the frameworks, please use this environment or its star-version \texttt{Ecoblock*} (without numbering).
For referencing to those boxes, use the \texttt{label}-option; to include a title, the \texttt{title}-option.
It is highly encouraged to start and\slash or end a chapter with an economic analogy.
%

%
If one of your boxes is a stand-alone-unit in your chapter which does not follow tightly in the flow of the main text (and if it is not larger than one page), you might also consider the \texttt{float} option.
It works similar to figure placement: Setting \texttt{float=\{th\}} will for example try to place the box on the top of the current or next page.
(If you don't know what this means, just refrain from using the \texttt{float} option.)
\end{Ecoblock}

\paragraph{Other ``boxes''} You may furthermore use

\hfill\parbox[t]{.45\columnwidth}{\begin{Philblock*}This is a \texttt{Philblock*}\end{Philblock*}}\mbox{~}%
\parbox[t]{.45\columnwidth}{\begin{Expblock}and this an \texttt{Expblock}\end{Expblock}}
\hfill\hfill

\hfill
\parbox[t]{.45\columnwidth}{\begin{MathDetailblock}\texttt{MathDetailblock} may be used for more complex mathematical reasoning that can be skipped on a first read of the chapter.\end{MathDetailblock}}
\mbox{~}%
\parbox[t]{.45\columnwidth}{\begin{chapterhighlights}\texttt{chapterhighlights} are already known.\end{chapterhighlights}}\hfill\hfill

\begin{Generalblock}
Finally, there is also a \texttt{Generalblock} for ``boxed content'' that does not fit into any of the other categories.
Note that the \texttt{Generalblock} has no starred (*) equivalent and no header line at all what not use with the \texttt{title} option.

{\color{gray}Here, we can refer back to Economic analogy~\ref{INSTRUCTIONS_LATEX:Ecoblock:intro}, which is just included to show how referencing boxes works.}
\end{Generalblock}

%
\section{Style guidelines}
\label{INSTRUCTIONS_LATEX:Sec:Style}

\subsection{General notes on language and style}
Please use American (US) English when writing the chapters. Please use the serial comma (as in ``A, B, and C''). Please use no comma after ``e.g.'' or ``i.e.''.
It is very likely that margin sizes, fonts, separation lengths, enumeration pattern and many more will be changed again.
So: Don't worry about wrong word hyphenations, over\slash underfull boxes in \LaTeX, long entries in the table-of-contents, \texttt{hyperref}-naming-conventions or similar issues when writing the chapters.


%
\subsection{Graphics and tables}

As said above, the overall layout has not been decided yet.
That means: \textbf{Don't} put too much effort into the graphics and tables now. (Scans of hand-drawn graphics are good enough.)
Maybe, we will (re-)do all graphics in one go, later in the process.
Figures should be half-page and\slash or full-page width and included inside a labeled \LaTeX{} \texttt{figure} environment that is referenced in the text.
Please use `common' graphic formats, preferably \texttt{pdf}, \texttt{jpg}, \texttt{png}, or \texttt{eps} and insert these using the \LaTeX{} command
\begin{center}
   \texttt{\textbackslash includegraphics[<key-value-list>]\{\textbackslash chapterroot <local\_path\_to\_image\_file>\}}.
\end{center}
Note that, in the case of \texttt{eps} files, the \texttt{shell-escape} option of \texttt{pdflatex} needs to be enabled.
If yopu don't know what that means, it is easier to not use \texttt{eps} for now.
If you want to use \LaTeX-specific drawing packages (tikz, pstricks or the picture-environment), you are welcome to do so.
%

%
If plots are included and the underlying numerical data are available, please also include the according data files.
The format of such a data file is not important, a simple \texttt{.dat} file with x and y (and z) columns, an Excel sheet, or Matlab \texttt{.fig} files all work well.

If necessary: Add copyright information\slash original sources when using graphics.
See also the \texttt{\textbackslash copyrightinfo} command below.

In \figurename~\ref{INSTRUCTIONS_LATEX:fig:workflow}, we visualize the general workflow of the chapter writing process, in \figurename~\ref{INSTRUCTIONS_LATEX:fig:Michaelis}, we use a \texttt{.jpg} image file, where additional copyright information is provided in a file with the same name and in the same folder, and finally \figurename~\ref{INSTRUCTIONS_LATEX:fig:someplot} shows some numerical (dummy) results.
It is also possible to include multiple images into one \texttt{figure} environment; so far without a standardized way to do it.
%
\begin{figure}[t!]% The figure environment is a "float" object. That means that LaTeX "decides" where to put it on the page. The optional parameters "h" (here), "t" (top of a page), "p" (on the same page), "b" (bottom of a page) describe your preferences to assist that decision.
  \begin{center}
    \includegraphics[scale=1]{\chapterroot images/workflow.pdf}
  \end{center}
  \caption{The proposed workflow of writing}
  \label{INSTRUCTIONS_LATEX:fig:workflow}
\end{figure}
%

%
\begin{figure}[t!]
  \begin{center}
    \includegraphics[width=0.25\columnwidth]{\chapterroot images/Leonor_Michaelis.jpg}
  \end{center}
  \caption{Leonor Michaelis, biochemist known for the law of enzyme kinetics
   \copyrightinfo{\url{https://upload.wikimedia.org/wikipedia/commons/2/28/Leonor_Michaelis.jpg}
    visited on 2021-10-02}
   }
  \label{INSTRUCTIONS_LATEX:fig:Michaelis}
\end{figure}
%

%
\begin{figure}[t!]
  \begin{center}
    \includegraphics[width=0.5\columnwidth]{\chapterroot images/someplot.eps}
  \end{center}
  \caption{Some random data points, note the \texttt{.fig} file and the provided \texttt{.dat} file in the subdirectory \texttt{./data}}
  \label{INSTRUCTIONS_LATEX:fig:someplot}
\end{figure}
%

\LaTeX{} tables can be tricky and quickly become complex.
So, the same rules as for graphics apply when it comes to tables:
\begin{enumerate}
  \item{Don't put too much effort in it, a screenshot of an Excel sheet will work fine for now.
  }
  \item{More important then the look and feel of the table is the content: Please provide \texttt{.csv} or \texttt{.xlsx} files with the data.
  }
\end{enumerate}
Use a \LaTeX{} \texttt{table} environment (a figure is also possible) when inserting tables in your manuscript and refer to them in the main text as in \tablename~\ref{INSTRUCTIONS_LATEX:tbl:some_data_in_a_table}.
%
\begin{table}[htpb]
 \begin{center}
  \parbox{.45\columnwidth}{\centering
  	(a)\\
  \begin{tabular}{ccc}
   \hline A & B & C \\
   \hline
         1 & 2 & 3 \\
         4 & 5 & 6 \\
         7 & 8 & 9 \\
    \hline
  \end{tabular}
  }
  \parbox{.45\columnwidth}{\centering
  (b)\\
  \includegraphics[width=9em]{\chapterroot images/Screenshot_of_table.png}
  }
 \end{center}
 \caption{(a) A table, (b) some data in a screenshot; the Excel sheet is in the chapter folder}
 \label{INSTRUCTIONS_LATEX:tbl:some_data_in_a_table}
\end{table}
%


% %%%%%%%%%%%%%%%%%%%%
\subsection{Labels}
\label{INSTRUCTIONS_LATEX:Subsection:labeling}

You are completely free in choosing the labels within your chapter as long as they start with the three letter code of your chapter.
Please use labels only (mainly) if you intent to reference to them in the text.

\paragraph{Example} You are an author within the chapter `\texttt{ABC}' and want to give a label to a \texttt{subsection} `Mathematical Background'.
Then, in your \LaTeX{} document, you add a line like
\begin{center}
	\texttt{\textbackslash subsection\{Mathematical Background\}
	\textbackslash label\{ABC:Mathsubsection\}}
\end{center}
and reference to this subsection at another point of your document like this
\begin{center}
	\texttt{As seen in Subsection \textbackslash ref\{ABS:Mathsubsection\}, the exponential function is key here...}
\end{center}
Cross-chapter references are to be avoided unless you are an author in two different chapters or agreed to doing so with the according chapter editors and the book editors.


\section{Bibliography references}
\label{INSTRUCTIONS_LATEX:Sec:Bibstyle}

At the beginning, each chapter can have its own bibliography (\texttt{.bibtex}) file and such a file is also part of the templates.
Later, when the (first) chapters are consolidated, we will merge these files and work with a central Bib\TeX{} file instead.
Always try to include a DOI to the Bib\TeX{} entry (in the Bib\TeX{} entry field “DOI”).
For Bib\TeX{} references, please use labels of the form WatsonCrick1953 (= first author last name + second author last name + year).
In case of ambiguity, please add an a, b, c, etc.
Common labels will be important for us to come up later with a central Bib\TeX{} file.

\begin{table}[htpb]
\begin{center}
\begin{tabular}{ccccc}
\hline
Reference & using \texttt{cite} & using \texttt{citep} & using \texttt{citet} & using \texttt{citeauthor}
\\ \hline
Einstein1905 & \cite{Einstein1905} & \citep{Einstein1905} & \citet{Einstein1905} & \citeauthor{Einstein1905}
\\
MichaelisMenten2013 & \cite{MichaelisMenten2013} & \citep{MichaelisMenten2013} & \citet{MichaelisMenten2013} & \citeauthor{MichaelisMenten2013}
\\
OrthThiele2010 & \cite{OrthThiele2010} & \citep{OrthThiele2010} & \citet{OrthThiele2010} & \citeauthor{OrthThiele2010}
\\
Einstein and Darwin & \cite{Einstein1905,Darwin2009} & \citep{Einstein1905,Darwin2009}& \citet{Einstein1905,Darwin2009}& \citeauthor{Einstein1905,Darwin2009}
\\
 \hline
\end{tabular}
\end{center}
\caption{The citation commands and their appearances}
\label{INSTRUCTIONS_LATEX:tbl:citations}
\end{table}

Do not worry about sorting the entries when citing multiple references at the same time.
The main \LaTeX{} files use the \texttt{natbib} package.
That means that several commands for citations are pre-defined.
We recommend authors to mainly use \texttt{citep} and \texttt{citeauthor}, cf.\ \tablename~\ref{INSTRUCTIONS_LATEX:tbl:citations}.
Specifically, when emphasizing one particularly important reference, and\slash or the one, the entire chapter is built on, use \texttt{\textbackslash citeauthor}.
%

%
Please do not include a bibliography, i.e.\ a \texttt{\textbackslash bibliography}, a \texttt{\textbackslash printbibliography}, or a \texttt{\{thebibliography\}} in your \texttt{chapter.tex} file.
It is setup such that all your Bib\TeX{} entries are automatically collected in one such list if \texttt{chapter.tex} is compiled on its own.

%
\section{Typesetting formulas}
\label{INSTRUCTIONS_LATEX:Sec:Formualae}


\subsection{Formula environments}
When using inline formulas, use the \LaTeX{} \texttt{\$...\$} definition; for displaystyle formulas, we recommend
\begin{itemize}
  \item the environments \texttt{equation} and \texttt{equation*} for single-line formulas with or without labeling tags,
  \item the environments \texttt{split}, \texttt{align}, and \texttt{align*} for multi-line formulas, and
  \item the \texttt{subequations} environment for grouped enumerated equations.
\end{itemize}
For a consolidated look-and-feel, we encourage the use of these and \textbf{not} \texttt{gather}, \texttt{multline}, \texttt{eqnarray} and so on.
%

%
For example, the Pythagorean theorem can be stated inline like this: $a^2 + b^2 = c^2$, or as a formula without a tag:
\begin{equation*}
a^2 + b^2 = c^2\,,
\end{equation*}
or with a tag, like this
\begin{equation}
\label{INSTRUCTIONS_LATEX:eq:Pythagoras}
a^2 + b^2 = c^2\,.
\end{equation}
For bigger calculations, \texttt{align} and \texttt{subequations} can be used like this:
\begin{subequations}%
\label{INSTRUCTIONS_LATEX:eq:Gauss}%
Suppose, you want to show that
\begin{equation}
\forall n \in \Natu: \sum_{k=1}^n k = \frac{n \cdot (n+1)}{2}\,.
\end{equation}
You can do this using mathematical induction.
The induction step includes
\begin{align}
\sum_{i=1}^{n+1} i &= \left(\sum_{i=1}^n i\right) + n+1
\notag
\\
  &= \frac{n \cdot (n+1)}{2} + n + 1 \notag
\\
  &= \frac{n \cdot (n+1) + 2 \cdot (n+1)}{2} \notag
\\
  &= \frac{(n+1)\cdot ((n+1)+1)}{2}\,.
\end{align}
\end{subequations}
Finally, referencing to equations is done using the \texttt{\textbackslash eqref} command, cf.\ \eqref{INSTRUCTIONS_LATEX:eq:Pythagoras} and \eqref{INSTRUCTIONS_LATEX:eq:Gauss}.

\subsection{Chemical formulas}
We recommend using the \texttt{mhchem} package, which is imported by default. The full documentation can be found \href{http://mirrors.ctan.org/macros/latex/contrib/mhchem/mhchem.pdf}{here}. Or, for a quick guide, here are a few examples:

\begin{itemize}
    \item \ce{^13 CO3^2-}
    \item \ce{C6H12O6 + 6 O2 -> 6 CO2 + 6 H2O}
    \item \ce{E + S <=>[k_1][k_2] ES <=>[k_3][k_4] EP <=>[k_5][k_6] E + P}
\end{itemize}


%
\subsection{Notational conventions}

We will use the following conventions concerning the symbols for multi-dimensional variables:
\begin{itemize}
  \item matrices: Bold, non-italic, capital (if possible: latin) letters: $\Amat$; when printed as a whole, we use round parentheses (e.g.\ \LaTeX-\texttt{pmatrix} environment) 
  \item vectors: Bold, non-italic, lower-case letters: $\avec$, $\varphivec$
  \item scalars: non-bold, italic, lower-case letters: $a$, $b$
  \item indices: italic (variable non-bold): $a_{i}$
  \item non-index subscripts or superscripts: non-italic (latex command \texttt{\textbackslash mathrm}): $a^{\mathrm{max}}_{i}, c_{\mathrm{ATP}}$
\end{itemize}

Indexing of vectors and\slash or matrices is done using subscripts starting with index $1$, i.e.\
\begin{equation}
\label{EXM:eq:use_of_split}
 \begin{split}
  \Real^n \ni \avec &= (a_i)_{i=1}^n \qquad \Real^m \ni \varphivec = (\varphi_j)_{j=1}^m
  \\
  \Real^{n \times m} \ni \Amat &= (a_{ij})_{i=1,\ldots,n,j = 1 , \ldots , m}
 \end{split}
\end{equation}
Note, how the \texttt{split} environment was used to set formula \eqref{EXM:eq:use_of_split}.
%

%
To define new macros for vector- or matrix-valued variables, we provide the macros \texttt{\textbackslash mathvectorfont} and \texttt{\textbackslash mathmatrixfont}.
If you wish to use the symbol ``\texttt{\textbackslash Nmat}'' as a shortcut for a matrix $\Nmat$, just add the line
\begin{center}
\texttt{\textbackslash newcommand\{\textbackslash Nmat\}\{\textbackslash mathmatrixfont\{N\}\}}
\end{center}
to your \texttt{macros-chapter.tex}.\\
%

Please avoid the use variable names consisting of several letters (such as $VOL$)!\\

In contrast, standard mathematical functions with several letters can be used and should be written as  non-italic (latex command \texttt{\textbackslash operatorname} and\slash or \texttt{\textbackslash mathop\{\textbackslash mathrm\{\}..\}}): $\operatorname{min}(a_{i})$, $\operatorname{arg\,min}(f)$.

%
Additionally, we provide the following macros
\begin{itemize}
  \item real numbers $\Real$, nonnegative integers $\Natu$, integers $\Inte$
  \item integrals, differentiation:  $\frac{\intd}{\intd t}$
  \item Eulers number $\expe$, imaginary unit $\imagi$.
\end{itemize}
The list will be extended and made available as a separate document later on.



% %%%%%%%%%%%%%%5
\section{Use of code or jupyter notebooks}

The infrastructure for accompanying Jupyter notebooks has not yet been decided on.
We recommend that you dedicate a subfolder to programming-related issues.
%

%
By default, the package \texttt{listings} is loaded which allows you to include source code via the environment \texttt{lstlisting} like this (see source code of the \LaTeX{} document)
\begin{lstlisting}
sum = 0 # store sum of integers here
for i in range(10):
	sum += i
print('The result is ', i)
\end{lstlisting}
or the macro \texttt{\textbackslash lstinputlisting[<options>]\{\textbackslash chapterroot <somefile.ending>\}}.
%

%
For customizing the style, you can use \texttt{\textbackslash lstset}.
We recommend, that you define the style of your code examples globally in the file \texttt{style-chapter.tex}.


% %%%%%%%%%%%%%%%%%%%%%
\section{Units and numerical data}

The main macro file includes the \texttt{siunitx} package.
That means in particular that the macros
\begin{itemize}
  \item \texttt{\textbackslash si\{<unit>\}},
  \item \texttt{\textbackslash SI\{<num>\}\{<unit>\}}, and
  \item \texttt{\textbackslash DeclareSIUnit[number-unit-product = \{\}]\{<new\_unit>\}\{<symbol>\}}
\end{itemize}
are available.
You can, for example, write (numbers with) units in the following way:
\begin{equation*}
[\si{\newton}] = [\si{\kilo\gram\metre\per\square\second}]
\qquad
\SI{1.0}{\calorie} = \SI{4.184}{\joule}.
\end{equation*}
If you feel comfortable to do so, we encourage you to use this package.



% %%%%%%%%%%%%%%%%%%%%
\section{Self-plagiarism and copyright}

It is okay to re-use material from other sources if you are sure that no copyright and\slash or trademarks are violated.
We provide the command
\begin{center}
 \texttt{\textbackslash copyrightinfo\{<some\_information\_on\_the\_source\_and/or\_usage>\}}
\end{center}
that should be included in the text whenever non-original material was used.
If a considerable amount of a chapter's content comes from a different source, please also note this at the beginning of the chapter's GoogleDoc.



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}

Each chapter should end with a short summary that refers back to questions from the introductions and\slash or the economic analogy.
Concerning the use of the templates, there is a simple rule: All \texttt{.tex} or \texttt{.bib} files that end in \texttt{*chapter} can freely be used and changed. (with the exception of especially marked sections at the beginning and end of the files)


%
\section*{Exercises}

\begin{exercise}\label{INSTRUCTIIONS_LATEX:Exerc:Use_ref_in_exercise}
If you wish to add exercises to your chapter, please use the \texttt{exercise} environment.
\end{exercise}


\begin{exercise}
You can reference to exercises like Exercise \ref{INSTRUCTIIONS_LATEX:Exerc:Use_ref_in_exercise} within your chapter using \texttt{\textbackslash ref}.
\end{exercise}





% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\section*{FAQ's}

What about an index, a glossary, common abbreviations, the `listoffigures' and so on?
\begin{flushright}
\comment{To be defined later on. Don't worry about that now.}
\end{flushright}

What about acronyms that I want to include?
\begin{flushright}
\comment{Add this information in the chapter's GoogleDoc.}
\end{flushright}

What about keywords that I want to include?
\begin{flushright}
\comment{Add this information using the \texttt{\textbackslash chapterkeywords} command.}
\end{flushright}

What about lecture material or tasks\slash questions for students?
\begin{flushright}
\comment{Add this information in the chapter's GoogleDoc.
Additional files can already be added to the git repository.
You may also consider using \texttt{exercise}-s to include such material.}
\end{flushright}

\hideinbooklayout{
\ \\
\noindent\href{https://creativecommons.org/licenses/by-sa/4.0/}{\includegraphics[width=2.5cm]{images/by-sa.png}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       please do not edit this block                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifx\weAreInMain\undefined                                                      %
\bibliographystyle{unsrtnat}                                                    %
\bibliography{\chapterroot bibliography-chapter}                                %
\end{document}                                                                  %
\fi                                                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

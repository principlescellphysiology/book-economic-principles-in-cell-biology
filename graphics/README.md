Graphics related to the book
============================

Chapter logos etc are stored in the git repository https://gitlab.com/principlescellphysiology/principles-cell-physiology (together with the HTML code of our website)

* The original svg files defining the chapter logos can be found in the git folder https://gitlab.com/principlescellphysiology/principles-cell-physiology/-/tree/master/public/images/chapter-logos

* The original svg file defining the chapter roadmap can be found in the git folder https://gitlab.com/principlescellphysiology/principles-cell-physiology/-/tree/master/public/book-economic-principles/images

* The Piglette animation for the website can be found in the gitlab folder
https://gitlab.com/principlescellphysiology/principles-cell-physiology/-/tree/master/public/book-economic-principles/animation

* Other Piglette pictures can be found in the gitlab folder
https://gitlab.com/principlescellphysiology/principles-cell-physiology/-/tree/master/open-graphics/piglette-graphics

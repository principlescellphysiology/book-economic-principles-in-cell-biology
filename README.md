# Book "Economic principles in cell biology"

<img src="https://principlescellphysiology.org/images/piggy_bancteria.png" alt="EPCP Logo" width="200"/>

Our book for researchers and students will describe the economics of cells from different angles, will condense knowledge we have as a community, and will serve as an open resource for learning and teaching. We

* write the first textbook / companion book on the topic, as an open and free book before commercial textbooks emerge (to be read online, as a pdf, and possibly to be printed on demand for non-profit)
* show that a useful book can be written and continuously updated by an collective of researchers and students, without limitations imposed by commercial publishers
* establish this as a standard book to which colleagues can later contribute rather than writing their own book on the topic

This is a community project - please join us anytime!

## This repository

This repository is the central place where manuscript files are assembled. Chapter authors do not use this git repository, but write their chapters on overleaf. 

* ```book-manuscript/latex``` Latex source code for compiling the book (using chapter latex files from overleaf repositories)
* ```book-manuscript/pdf``` Latest compiled version of book and individual chapters (pdf files)
* ```book-graphics```: graphics files directly related to the book project (logos, overview graphics)
* ```scripts``` Scripts for collecting chapter files and compiling the book
* ```website-releases``` Older versions of the book (published on website)

Please note: other materials related to the book (advertisement poster, jupyter notebooks, software tools) have been moved to the git repository https://gitlab.com/principlescellphysiology/principles-cell-physiology

## If you would like to write a new chapter

If you would like to start writing a chapter, please contact us. We will set up an overleaf project for your new chapter.

Our Latex Template files are available in the overleaf project https://www.overleaf.com/project/621f7ab65dc8bb069630b8ec.

## Useful links

For any further information and, please visit our [book website](https://principlescellphysiology.org/book-economic-principles/index.html) or our [website for authors](https://principlescellphysiology.org/book.html).

## Contact

Elad Noor, Markus Köbis, Wolfram Liebermeister

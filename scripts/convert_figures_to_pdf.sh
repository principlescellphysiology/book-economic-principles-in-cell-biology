#!/bin/bash

# I need this because I run inskape on a Mac, disable in Linux
if [[ "$OSTYPE" != "linux-gnu"* ]]; then
	echo "this script only works on Linux and requires 'inkscape' and 'epstopdf' to be in PATH"
    exit
fi

for file in ../book-manuscript/latex/chapters/*/vector_graphics/*.svg; do
    outfile="${file%.svg}.pdf"
    outfile="${outfile/vector_graphics/images}"
    echo "converting ${file} to $outfile"
    inkscape "$file" --export-filename="$outfile"
done

for file in ../book-manuscript/latex/chapters/*/vector_graphics/*.eps; do
    outfile="${file%.eps}.pdf"
    outfile="${outfile/vector_graphics/images}"
    echo "converting ${file} to $outfile"
    epstopdf "$file" --outfile="$outfile"
done


Script: download all draft google docs as pdfs and merge to one pdf
-------------------------------------------------------------------

Usage: go to a working directory of your choice and run

```sh download_all_draft_google_docs.sh```

This will download all draft google docs as pdfs and merge them into one large pdf (and generate a second version of it, with two pages per page). All files are saved to the current working directory.

Please note:
* To enable this, the google docs need to have "View" rights granted to everyone!
* To convert google docs to latex, use the commands described at the botton of this file (this does not work great, we can improve it when we need it)
* The file "wrapper.tex" is needed to convert google doccs to latex; this has not been fully implemented yet, if you need this, please contact Wolf

Contact for questions: Wolf

# ---------------------------------------------------------------
# Download all draft google docs as pdfs and merge to one pdf
#
# NOTE: To enable this, the google docs need to have "View" rights granted to everyone!
# NOTE: To convert google docs to latex, use the commands described at the botton of this file
#       This does not work great, we can improve it when we need it
#
# Contact for questions: Wolf
# ---------------------------------------------------------------

# Chapter preface (PRE)
# https://docs.google.com/document/d/1KZyTtwX1boF3B8C2RFkNsLCauXHk5U6htzYKNeawM4A/edit
wget "https://docs.google.com/document/d/1KZyTtwX1boF3B8C2RFkNsLCauXHk5U6htzYKNeawM4A/export?format=pdf" -O chapter_PRE.pdf

# Chapter Cell as a factory (FAC)
# https://docs.google.com/document/d/1eSRDl5RMDaMDPDMT6q7nkRWn8LkLzZhNT55pLvc1Bhg/edit
wget "https://docs.google.com/document/d/1eSRDl5RMDaMDPDMT6q7nkRWn8LkLzZhNT55pLvc1Bhg/export?format=pdf" -O chapter_FAC.pdf

# Chapter Optimal choices in cells (CMO) 
# https://docs.google.com/document/d/1ZP6Szvjl8QL7WingKeD1fpWUoWdVjmHLgTW1rFLAkZE/edit#
wget "https://docs.google.com/document/d/1ZP6Szvjl8QL7WingKeD1fpWUoWdVjmHLgTW1rFLAkZE/export?format=pdf" -O chapter_CMO.pdf

# Chapter Understanding living cells (THE) 
# https://docs.google.com/document/d/1FhYM55-l2qy47HyUweSUUu4_M7nf-XDTjrgbKRrgWlA/edit#
wget "https://docs.google.com/document/d/1FhYM55-l2qy47HyUweSUUu4_M7nf-XDTjrgbKRrgWlA/export?format=pdf" -O chapter_THE.pdf

# Chapter What makes up a cell (CEN) 
# https://docs.google.com/document/d/1iTXW9oZJzJAe2PzAfZhCFJpwlQC-PTGlg2QbGTiX5bY/edit?
wget "https://docs.google.com/document/d/1iTXW9oZJzJAe2PzAfZhCFJpwlQC-PTGlg2QbGTiX5bY/export?format=pdf" -O chapter_CEN.pdf

# Chapter A dynamic picture of metabolism (MET) 
# https://docs.google.com/document/d/1mVb-2qCP-CMNTKZxxCvM9jcSlemntIeM257TTeQkT6I/edit#
wget "https://docs.google.com/document/d/1mVb-2qCP-CMNTKZxxCvM9jcSlemntIeM257TTeQkT6I/export?format=pdf" -O chapter_MET.pdf

# Chapter Balanced growth (BAL) 
# https://docs.google.com/document/d/1l2qXe8qZu3Qs-g2R6h61VkthD9E-LQUDYNW76TwDOMA/edit
wget "https://docs.google.com/document/d/1l2qXe8qZu3Qs-g2R6h61VkthD9E-LQUDYNW76TwDOMA/export?format=pdf" -O chapter_BAL.pdf

# Chapter Optimality problems in biology (OPT) 
# https://docs.google.com/document/d/1SwpVKXl03dBOBNhWyXFRoeB2n4QH3AMAmfcqGpe79A0/edit#
wget "https://docs.google.com/document/d/1SwpVKXl03dBOBNhWyXFRoeB2n4QH3AMAmfcqGpe79A0/export?format=pdf" -O chapter_OPT.pdf

# Chapter The space of metabolic flux distributions (CBM) 
# https://docs.google.com/document/d/1T23ugMud0tV_5B7DsiDGNzHFuTEMyPSyCYgxCGQMKoE/edit?usp=sharing
wget "https://docs.google.com/document/d/1T23ugMud0tV_5B7DsiDGNzHFuTEMyPSyCYgxCGQMKoE/export?format=pdf" -O chapter_CBM.pdf

# Chapter Optimization in flux space (FLX) 
# https://docs.google.com/document/d/14sY1xkRZBAAFKSxIY9juTPGPUAKV6e-IJ5Byvy5qItI/edit?usp=sharing
wget "https://docs.google.com/document/d/14sY1xkRZBAAFKSxIY9juTPGPUAKV6e-IJ5Byvy5qItI/export?format=pdf" -O chapter_FLX.pdf

# Chapter The choice between metabolic pathways (PAT) 
# https://docs.google.com/document/d/1QSemfqEiO9QAawwRZngNcZUomQeDCe_jcYpmc88bcFA/edit
wget "https://docs.google.com/document/d/1QSemfqEiO9QAawwRZngNcZUomQeDCe_jcYpmc88bcFA/export?format=pdf" -O chapter_PAT.pdf

# Chapter Enzyme-efficient metabolic states (OME) 
# https://docs.google.com/document/d/1WcGTahQ7i9l2BrYC9lKh7iazSGvPBiLZYcSLA6__5IA/edit?usp=sharing
wget "https://docs.google.com/document/d/1WcGTahQ7i9l2BrYC9lKh7iazSGvPBiLZYcSLA6__5IA/export?format=pdf" -O chapter_OME.pdf

# Chapter Optimal states and metabolic control (MCA) 
# https://docs.google.com/document/d/1Je5jNb0WRmI3AyO6VaH8tZq8w3a-OLIO1bxYIeSAxQI/edit#
wget "https://docs.google.com/document/d/1Je5jNb0WRmI3AyO6VaH8tZq8w3a-OLIO1bxYIeSAxQI/export?format=pdf" -O chapter_MCA.pdf

# Chapter Coarse grained models of cellular growth (SMA) 
# https://docs.google.com/document/d/1PsnCOqcNXjevCWh2wm_RxsJK_yMHyS4hkvvnV-3XUJo/edit#
wget "https://docs.google.com/document/d/1PsnCOqcNXjevCWh2wm_RxsJK_yMHyS4hkvvnV-3XUJo/export?format=pdf" -O chapter_SMA.pdf

# Chapter Large detailed cell models (LAR) 
# https://docs.google.com/document/d/1eLhoCmobFU9zZAdK5klLd-5xs9h0xrqA0iqiPxOZJVo/edit
wget "https://docs.google.com/document/d/1eLhoCmobFU9zZAdK5klLd-5xs9h0xrqA0iqiPxOZJVo/export?format=pdf" -O chapter_LAR.pdf

# Chapter Costs and benefits of protein expression (PRF) 
# https://docs.google.com/document/d/1RTwQ8ScPZc86lGwQ1rKfpu8uumLIhImYsG1MRKly6Y0/edit
wget "https://docs.google.com/document/d/1RTwQ8ScPZc86lGwQ1rKfpu8uumLIhImYsG1MRKly6Y0/export?format=pdf" -O chapter_PRF.pdf

# Chapter Optimal cell behavior in time (TIM) 
# https://docs.google.com/document/u/0/d/1YegyOFji0KthuEFKYN1n6igECavcw3WkSnTeFXm0dNs/edit
wget "https://docs.google.com/document/u/0/d/1YegyOFji0KthuEFKYN1n6igECavcw3WkSnTeFXm0dNs/export?format=pdf" -O chapter_TIM.pdf

# Chapter Optimal regulation (REG) 
# https://docs.google.com/document/d/1DWk1JMtIGYDhUeD6LnY_4F8LtwhQicTNv55OPfPma_w/edit#
wget "https://docs.google.com/document/d/1DWk1JMtIGYDhUeD6LnY_4F8LtwhQicTNv55OPfPma_w/export?format=pdf" -O chapter_REG.pdf

# Chapter Metabolic diversity in cell populations (VAR) 
# https://docs.google.com/document/u/0/d/1gEoXaUOoeknI6YROtf86S2Fhul-A00JStp6swVeOTwY/edit
wget "https://docs.google.com/document/u/0/d/1gEoXaUOoeknI6YROtf86S2Fhul-A00JStp6swVeOTwY/export?format=pdf" -O chapter_VAR.pdf

# Chapter Control of cell division and coordination with other cell-cycle processes (CDC) 
# https://docs.google.com/document/d/1aJEnyHqvmQ4izrbhJEhGRY6HskTOQkoxGuQA7IguAeA/edit#
wget "https://docs.google.com/document/d/1aJEnyHqvmQ4izrbhJEhGRY6HskTOQkoxGuQA7IguAeA/export?format=pdf" -O chapter_CDC.pdf

# Chapter Cell strategies under uncertainty (UNC) 
# https://docs.google.com/document/d/1d4WVGXr3OwAs2gZDNV4cnQpGzsAzIymkohzHEaG62xk/edit#heading=h.het49ygibrdh
wget "https://docs.google.com/document/d/1d4WVGXr3OwAs2gZDNV4cnQpGzsAzIymkohzHEaG62xk/export?format=pdf" -O chapter_UNC.pdf

# Chapter Population and community models (POP) 
# https://docs.google.com/document/d/1TJoPbdWEdSZ_S1nLg1ov7LTgHNKKJ0MZa2Fa8DgLnwk/edit#
wget "https://docs.google.com/document/d/1TJoPbdWEdSZ_S1nLg1ov7LTgHNKKJ0MZa2Fa8DgLnwk/export?format=pdf" -O chapter_POP.pdf

# Chapter Game theory in biology (GAM) 
# https://docs.google.com/document/d/1UTgCOjLkx-ZuQbs-A2YJpYp3GcFS0im0BC9wrCgHrxE/edit#
wget "https://docs.google.com/document/d/1UTgCOjLkx-ZuQbs-A2YJpYp3GcFS0im0BC9wrCgHrxE/export?format=pdf" -O chapter_GAM.pdf

# Chapter Return on investment in cellular metabolism and interspecies interactions (ROI) 
# https://docs.google.com/document/d/1l4iKZPytl654hErUQX_DwPfogFA5VtAZx2oJ1WwKqYY/edit#
wget "https://docs.google.com/document/d/1l4iKZPytl654hErUQX_DwPfogFA5VtAZx2oJ1WwKqYY/export?format=pdf" -O chapter_ROI.pdf

# Chapter Economy of organ shapes and function (ORG) 
# https://docs.google.com/document/d/1nat0d0zfhFqQZRdeW_3EyDphPdyu23hiZaFXU8z0HNs/edit
wget "https://docs.google.com/document/d/1nat0d0zfhFqQZRdeW_3EyDphPdyu23hiZaFXU8z0HNs/export?format=pdf" -O chapter_ORG.pdf

# Chapter Evolution, fitness, and optimality (FIT) 
# https://docs.google.com/document/d/1_euXpnLXcnV7mgjxvadwCyKhzwbIUMjVhSqX79a_bTs/edit#heading=h.ytjkf8y9cix9
wget "https://docs.google.com/document/d/1_euXpnLXcnV7mgjxvadwCyKhzwbIUMjVhSqX79a_bTs/export?format=pdf" -O chapter_FIT.pdf

# Chapter Economy of the cell (ECO) 
# https://docs.google.com/document/d/1PtfskrHKIIs16jQHpECNP0Oc-mKSC4gvI6V1X8IlqBE/edit#heading=h.het49ygibrdh
wget "https://docs.google.com/document/d/1PtfskrHKIIs16jQHpECNP0Oc-mKSC4gvI6V1X8IlqBE/export?format=pdf" -O chapter_ECO.pdf


# ---------------------------------------------------------------
# Combine all pdfs
# ---------------------------------------------------------------

pdfunite chapter_PRE.pdf chapter_FAC.pdf chapter_CMO.pdf chapter_THE.pdf chapter_CEN.pdf chapter_MET.pdf chapter_BAL.pdf chapter_OPT.pdf chapter_CBM.pdf chapter_FLX.pdf chapter_PAT.pdf chapter_OME.pdf chapter_MCA.pdf chapter_SMA.pdf chapter_LAR.pdf chapter_PRF.pdf chapter_TIM.pdf chapter_REG.pdf chapter_VAR.pdf chapter_CDC.pdf chapter_UNC.pdf chapter_POP.pdf chapter_GAM.pdf chapter_ROI.pdf chapter_ORG.pdf chapter_FIT.pdf chapter_ECO.pdf BOOK_ALL_CHAPTER_DRAFTS.pdf

# two pages on one

pdfnup --nup 2x1 --suffix doublepage BOOK_ALL_CHAPTER_DRAFTS.pdf 



# ---------------------------------------------------------------
# Alternative: for translation to latex (some problems, does not work well with formulae ..)
# ---------------------------------------------------------------

# Download to docx
# wget "https://docs.google.com/document/d/1KZyTtwX1boF3B8C2RFkNsLCauXHk5U6htzYKNeawM4A/export?format=docx" -O result.docx
# use pandoc to translate to latex
# pandoc -f docx -t latex result.docx > result.tex

# compile wrapper document that loads latex packages and the "result.tex" file (by \input command)
# pdflatex wrapper.tex

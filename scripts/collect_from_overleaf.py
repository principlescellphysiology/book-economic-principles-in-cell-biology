import os
import shutil
import git
import sys
import time
from path import Path

import pandas as pd
import hashlib as hl
import argparse

SCRIPT_DIR = Path(__file__).parent
DEST_PATH = SCRIPT_DIR.parent / "book-manuscript" / "latex" / "chapters"


parser = argparse.ArgumentParser()
parser.add_argument("-o", "--offline", help="work in offline mode (no Git operations)", action="store_true")
parser.add_argument("-u", "--update_chapter", help="update only a specific chapter from Overleaf", type=str, default="")
parser.add_argument("-d", "--dest_path", help="destination folder (where to copy the files to)", default=str(DEST_PATH))
args = parser.parse_args()

dest_path = Path(args.dest_path)
location_df = pd.read_csv(SCRIPT_DIR / "locations.csv", index_col=0)

for row in location_df.itertuples():
    sys.stderr.write(f"\n\nChapter {row.Index}\n-----------\n")
    
    if pd.isnull(row.overleaf_hash):
        sys.stderr.write(f"> not managed on Overleaf, skipping...\n")
        continue
    else:
        sys.stderr.write(f"> managed on Overleaf: {row.overleaf_hash}\n")
    
    git_dir = SCRIPT_DIR / "tmp" / row.overleaf_hash

    if args.offline:
        sys.stderr.write("> working offile, using local cached version folder {git_dir}\n")
    else:
        if not args.update_chapter or args.update_chapter == row.Index:
            if os.path.exists(git_dir):
                sys.stderr.write(f"> pulling latest version into local cache folder {git_dir}\n")
                g = git.cmd.Git(git_dir)
                g.pull()
            else:
                sys.stderr.write(f"> cloning repository into local cache folder {git_dir}\n")
                git.Repo.clone_from("https://git.overleaf.com/" + row.overleaf_hash, git_dir, branch="master", bare=False)
            sys.stderr.write("> waiting 10 seconds to avoid exceeding Overleaf's rate limit\n")
            time.sleep(10)

    sys.stderr.write(f"> copying files for chapter {row.Index}\n")
    shutil.copy(git_dir / "chapter.tex", dest_path / row.Index / "chapter.tex")
    shutil.copy(git_dir / "bibliography-chapter.bib", dest_path / row.Index / "bibliography-chapter.bib")
    shutil.copy(git_dir / "macros-chapter.tex", dest_path / row.Index / "macros-chapter.tex")
    shutil.copytree(git_dir / "images", dest_path / row.Index / "images", dirs_exist_ok=True)
    shutil.copytree(git_dir / "vector_graphics", dest_path / row.Index / "vector_graphics", dirs_exist_ok=True)
    if not pd.isnull(row.appendix):
        shutil.copy(git_dir / row.appendix, dest_path / row.Index / "appendix.tex")
    if not pd.isnull(row.solutions):
        shutil.copy(git_dir / row.solutions, dest_path / row.Index / "solutions.tex")

